<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentSettingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentSettingsTable Test Case
 */
class PaymentSettingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentSettingsTable
     */
    public $PaymentSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payment_settings',
        'app.users',
        'app.roles',
        'app.comments',
        'app.posts',
        'app.likes',
        'app.email_notifications',
        'app.feature_models',
        'app.followers',
        'app.subscriptions',
        'app.payments',
        'app.tweet_notifications',
        'app.user_medias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentSettings') ? [] : ['className' => PaymentSettingsTable::class];
        $this->PaymentSettings = TableRegistry::get('PaymentSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentSettings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
