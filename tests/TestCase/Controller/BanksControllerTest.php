<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BanksController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BanksController Test Case
 */
class BanksControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banks',
        'app.users',
        'app.roles',
        'app.comments',
        'app.posts',
        'app.likes',
        'app.email_notifications',
        'app.feature_models',
        'app.followers',
        'app.subscriptions',
        'app.payments',
        'app.tweet_notifications',
        'app.user_medias'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
