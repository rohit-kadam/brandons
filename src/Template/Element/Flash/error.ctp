<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger alert-dismissible" onclick="this.classList.add('hidden');">
	<button type="button" class="close custom-close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4 class="error-msgtext"><i class="icon fa fa-ban"></i> Error! <?= $message ?></h4>
	
</div>