
    <div class="col-md-3 static nopad-left">
      <div class="suggestions is_stuck" id="sticky-sidebar">
        <h4 class="grey recommend-txt"><?php echo $this->Html->image('recommended.svg',['class'=>'icon-rtfix img-responsive img-circle direct-recommend']); ?>  Recommended for you</h4>
          <?php if($recommended_users->count()>0) {?>
            
            <?php foreach ($recommended_users as $key => $value) {
              echo '<div class="outer-recomm-blk"><div class="follow-user1">';
               echo '<a href="'.$this->request->webroot.$value['username'].'">'.$this->Html->image('profile/'.$value['user_medias'][0]['face_pics'],['class'=>'profile-photo-sm img-responsive pull-left']).'</a>';
               echo $this->Html->image('bg-red.jpg',['class'=>'img-responsive imgban-bg']);
               echo '<div class="inner-recomblock">';
              echo '<div class="trecomnduser">
                  <h5 class="right-panel-head2 text-center"><a href="'.$this->request->webroot.$value['username'].'">@'.$value['username'].'</a></h5>
                  <div class="recommend-ft-sec"><div class="col-sm-4 col-xs-4 nopad-both border-right"><h5 class="description-header text-center">'.$this->requestAction('/Subscriptions/gettotalfollowers',array('id'=>$value['id'])).'</h5><span class="description-text">Followers|</span></div><div class="col-sm-4 col-xs-4 nopad-both"><a href="'.$this->request->webroot.$value['username'].'" class="btn btn-default subscribe_btn">Subscribe</a></div><div class="col-sm-4 col-xs-4 nopad-both"><span class="attheratetxt">| '.$this->Number->currency($value['amount'],"USD").'</span></div></div>
                  </div>
              </div>
              </div>
            </div>';
            }?>
            
          <?php }else{ ?>
          <p> Recommended model not available. </p>
        <?php } ?>
      </div>
    </div>