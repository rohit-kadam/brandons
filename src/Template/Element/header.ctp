
<header id="header">
    <nav class="navbar navbar-default navbar-default-cust navbar-fixed-top menu-cust">
        <div class="container">

          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header navbar-header-cust">
            <button type="button" class="navbar-toggle navbar-toggle-cust collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="mainlogo-text navbar-brand navbarb-cust" href="<?= $this->Url->build(['controller'=>'Users','action'=>'index'])?>"><?php echo $this->Html->image('bc_logo.png',['class'=>'img-responsive logo-img-style']); ?></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
	        <div class="collapse navbar-collapse navbar-collapse-cust" id="bs-example-navbar-collapse-1">
	            <ul class="nav navbar-nav navbar-right main-menu">
			          <!-- Messages: style can be found in dropdown.less-->
			        <li class="messages-menu right-space"><a href="<?= $this->Url->build(['controller'=>'Users','action'=>'editprofile',$this->request->session()->read('Auth.User.id')])?>"><p class="btm0">My Profile</p> <span class="fa fa-user-o displayblock text-center ficon_cmn"></span></a></li>
			        <li class="right-space"><a href="<?= $this->Url->build(['controller'=>'Users','action'=>'setting'])?>"><p class="btm0">My Settings</p> <span class="fa fa-cog displayblock text-center ficon_cmn"></span></a></li>
			        <li class="right-space"><a href="#"><p class="btm0">Add Card</p> <span class="fa fa-credit-card displayblock text-center ficon_cmn"></span></a></li>
			        <li class="right-space"><a href="#"><p class="btm0"> Messages</p> <span class="fa fa-whatsapp displayblock text-center ficon_cmn"></span></a></li>
			        <li class="right-space"><a href="<?= $this->Url->build(['controller'=>'notifications','action'=>'index'])?>"><p class="btm0">Notification</p> <span class="fa fa-users displayblock text-center ficon_cmn"></span></a></li>
			        <li class="dropdown notifications-menu dropdown-cust">
			            <a href="#" class="dropdown-toggle toggle-cmn" data-toggle="dropdown">
			              <i class="fa fa-bell-o fa-head-cust"></i>
			              <span class="label label-warning label-cust">10</span>
			            </a>
			            <ul class="dropdown-menu dropdown-menu-cust drop-menu-cmn">
			            	<div class="arrow"></div>

			              <li class="header">You have 10 notifications</li>
			              <li>
			                <!-- inner menu: contains the actual data -->
			                <ul class="menu menu-cust">
			                  <li>
			                    <a href="#">
			                      <i class="fa fa-users text-aqua "></i> 5 new members joined today
			                    </a>
			                  </li>
			                </ul>
			              </li>
			              <li class="footer footer-msgli"><a href="#">View all</a></li>
			            </ul>
			        </li>
			        <li class="dropdown user user-menu dropdown-cust">
			            <a href="#" class="dropdown-toggle toggle-cmn" data-toggle="dropdown">
			            	
			            	<?php if(in_array($this->request->session()->read('Auth.User.role_id'),[1,2,3])){
			            		echo $this->Html->image('profile/'.$face_pics,['class'=>'img-responsive user-imag user-img-space']);
			            		}else{
			            		echo $this->Html->image('user2-160x160.jpg',['class'=>'img-responsive user-imag user-img-space']);
			            		}?>

			              <span class="hidden-xs logouttext"><?= ucwords($this->request->session()->read('Auth.User.fullname'));?></span>
			            </a>
			            <ul class="dropdown-menu dropdown-menu-cust1 btm-space drop-menu-cmn">
			              <!-- User image -->
			              <div class="arrow"></div>
			              <!-- Menu Footer-->
				            <li class="user-footer">
				                  <a href="<?= $this->Url->build(['controller'=>'Users','action'=>'editprofile', $this->request->session()->read('Auth.User.id')])?>"><?php echo $this->Html->image('myprofile.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> My Profile</a>
				            </li>		

				            <?php if($this->request->session()->read('Auth.User.role_id')==2): ?>	            
				            <li class="user-footer">
				                  <a href="<?= $this->Url->build(['controller'=>'Subscriptions','action'=>'fans'])?>"><?php echo $this->Html->image('fans.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> Followers</a>
				            </li>
				            <?php endif; ?>	            

				            <?php if(in_array($this->request->session()->read('Auth.User.role_id'),[2,3])): ?>

				            <li class="user-footer"><a href="<?= $this->Url->build(['controller'=>'Subscriptions','action'=>'followings'])?>">
				            <?php echo $this->Html->image('following.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?><span>Following</span></a></li>
				            <li class="user-footer"><a href="#"><?php echo $this->Html->image('referrals.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> My Referrals</a></li>
				            <li class="user-footer"><a href="<?= $this->Url->build(['controller'=>'Users','action'=>'setting'])?>"><?php echo $this->Html->image('settings.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> My Settings</a></li>
				            <li class="user-footer"><a href="#"><?php echo $this->Html->image('statement.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> My Statements</a></li>
				            <li class="user-footer"><a href="#"><?php echo $this->Html->image('addcard.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> Add Card (to subscribe)</a></li>
				            <li class="user-footer"><a href="#"><?php echo $this->Html->image('bank.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> Add Bank (to earn)</a></li>
				            <?php endif; ?>	            

				            <li class="user-footer"><a href="<?= $this->Url->build(['controller'=>'Users','action'=>'logout'])?>"><?php echo $this->Html->image('logout.svg',['class'=>'icon-wfix img-responsive img-circle direct-chat-img1']); ?> Logout</a></li>
			            </ul>
			        </li>
	            </ul>
	        </div>
        </div><!-- /.container -->
    </nav>
</header>

