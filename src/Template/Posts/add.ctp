<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Comments'), ['controller' => 'Comments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Comment'), ['controller' => 'Comments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<!-- <div class="posts form large-9 medium-8 columns content">
    <?= $this->Form->create($post,['enctype'=>'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Add Post') ?></legend>
        <?php
            //echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('post_type',['options'=>['POST'=>'POST','PHOTOS'=>'PHOTOS','VIDEOS'=>'VIDEOS']]);
            echo $this->Form->control('content');
            echo $this->Form->control('media',['type'=>'file']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div> -->

<div class="posts form large-9 medium-8 columns content">
<?= $this->Form->create($post,['enctype'=>'multipart/form-data','id'=>'createpostForms']) ?>
<?= $this->Form->control('post_type',['options'=>['POST'=>'POST','PHOTOS'=>'PHOTOS','VIDEOS'=>'VIDEOS']]); ?>
<div class="col-xs-12" id="post_content">
<?= $this->Html->image('profile/'.$model->user_medias[0]['face_pics'],['class'=>'profile-photo img-responsive profile-photo-md'])?>
  <div class="textarea_wrap">
  <?php echo $this->Form->control('content',['type'=>'textarea','class'=>'form-control textarea-cust','placeholder'=>'What`s on your mind?','label'=>false,'id'=>'content']); ?>
  </div>
</div>
<div class="col-xs-12 text-right">
   <?= $this->Form->button(__('Post'),['class'=>'btn btn-danger btn-publish']) ?>
</div>
 <?= $this->Form->end() ?>
</div>                         