<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FeatureModel $featureModel
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Feature Model'), ['action' => 'edit', $featureModel->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Feature Model'), ['action' => 'delete', $featureModel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $featureModel->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Feature Models'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Feature Model'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="featureModels view large-9 medium-8 columns content">
    <h3><?= h($featureModel->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $featureModel->has('user') ? $this->Html->link($featureModel->user->id, ['controller' => 'Users', 'action' => 'view', $featureModel->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($featureModel->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($featureModel->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($featureModel->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($featureModel->modified) ?></td>
        </tr>
    </table>
</div>
