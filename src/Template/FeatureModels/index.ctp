<div class="container">
    <div class="row">
        <div class="box box-default box-top-sm">
            <div class="box-header with-header box-header-cust">
                <h3><?= __('Users') ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Fullname') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('posts') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>   
                        <?php $i=($this->Paginator->current()*10+1)-10; ?>    
                       <?php foreach ($featureModels as $featureModel): ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $this->Html->link(ucwords($featureModel->full_name), ['controller' => 'Users', 'action' => 'view', $featureModel->id]) ?></td>
                            <td><?= count($featureModel->posts) ?></td>
                            <td class="actions">
                                <?= $this->Html->link($featureModel->is_feature_models, ['controller'=>'Users','action' => 'updatefeaturemodel', $featureModel->id]) ?>
                               
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->Awesome->getPagination() ?>
            </div>
        </div>
    </div>
</div>




