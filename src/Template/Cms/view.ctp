<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cm $cm
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cm'), ['action' => 'edit', $cm->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cm'), ['action' => 'delete', $cm->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cm->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cms'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cm'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cms view large-9 medium-8 columns content">
    <h3><?= h($cm->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($cm->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cm->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($cm->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Content') ?></h4>
        <?= $this->Text->autoParagraph(h($cm->content)); ?>
    </div>
</div>
