	
<div class="container">
    <div class="row">
        <div class="box box-default box-top-sm">
            <div class="box-header with-header box-header-cust">
                <h3 class="inline-block"><?= __('Notifications') ?></h3>
            </div>
            <div class="box-body">
            	<?= $this->Awesome->getNotification("subscribed",$notifications); ?>
            </div>
        </div>
    </div>
</div>
