<?php $this->assign('title', 'Reset password');?>

<?= $this->Flash->render() ?>
<div class="brandons-outerbox1">

      <div  class="brandons-innerbox">
         <div class="banner-top">

            <div class="panel with-nav-tabs panel-cust">
                <div class="panel-heading panel-heading-cust">
                      <ul class="nav nav-tabs bc-tabs">
                          <li class="active model-litabs"><a href="#tab1danger" data-toggle="tab">Reset password</a></li>
                      </ul>
                </div>
                <div class="panel-body panel-body-cust">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1danger">        
                             <?php echo $this->Form->create('User', array('url' => array('controller' => 'users','action' => 'reset',$token))); ?>
                  <div class="form-group">
                  <i class="fa fa-envelope"></i>
                        <?= $this->Form->input('password',array('type'=>'password','label'=>false,'class'=>'form-control','placeholder'=>'Password')) ?>
                                        
                                        </div>
                                      <div class="form-group">
                                        <i class="fa fa-envelope"></i>
                        <?= $this->Form->input('password_confirm',array('type'=>'password','label'=>false,'class'=>'form-control','placeholder'=>'Confirm Password')) ?>
                                        
                                        </div>
                                        <div class="form-actions">
                      <?= $this->Form->button('Reset password',array('class' => 'btn btn-danger btn-block btn-flat','div' => false)) ?>
                                        
                                        </div>
                                         <?= $this->Form->end() ?>
                        </div>
                        <div class="login-helpers">
                        <a href="<?php echo $this->Url->build(['controller'=>'Users','action'=>'login']) ?>">Back to Login</a> <br>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

