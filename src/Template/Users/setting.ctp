<style type="text/css">
	/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.top100{margin-top:100px;}
</style>
<div class="container top100">
    <div class="box box-info col-md-12 col-xs-12 col-sm-12">
        <div class="box-header with-border">
          <h3 class="box-title">Email Notification</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                      <th>Notification Services</th>
                      <th>On/Off</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                      <td>New Subscriber</td>
                      <td>            
                          <label class="switch">
                            <input type="checkbox" <?= (isset($toggleNotify['New Subscriber'])?$toggleNotify['New Subscriber']:""); ?> onchange="ontriger(this.name)" name="newsubscription">
                            <span class="slider round"></span>
                          </label>
                      </td>
                    </tr>
                    <tr>
                      <td>Subscription Prolong</td>
                      <td>
                        <label class="switch">
                          <input type="checkbox" <?= (isset($toggleNotify['Subscriber Prolong'])?$toggleNotify['Subscriber Prolong']:""); ?> onchange="ontriger(this.name)" name="subscriptionprolong">
                          <span class="slider round"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>New Tip</td>
                      <td>
                        <label class="switch">
                          <input type="checkbox" <?= (isset($toggleNotify['New Tip'])?$toggleNotify['New Tip']:""); ?> onchange="ontriger(this.name)" name="newtip">
                          <span class="slider round"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>New Private Message</td>
                      <td>
                        <label class="switch">
                          <input type="checkbox" <?= (isset($toggleNotify['New Private Message'])?$toggleNotify['New Private Message']:""); ?> onchange="ontriger(this.name)" name="newprivatemessage">
                          <span class="slider round"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>New Referral</td>
                      <td>
                        <label class="switch">
                          <input type="checkbox" <?= (isset($toggleNotify['New Referral'])?$toggleNotify['New Referral']:""); ?> onchange="ontriger(this.name)" name="newreferral">
                          <span class="slider round"></span>
                        </label>
                      </td>
                    </tr>
                    <tr>
                      <td>New Posts Email Summary</td>
                      <td>            
                        <label class="switch">
                          <input type="checkbox" <?= (isset($toggleNotify['New post email summery'])?$toggleNotify['New post email summery']:""); ?> onchange="ontriger(this.name)" name="newpostemail">
                          <span class="slider round"></span>
                        </label>  
                      </td>
                    </tr>
                </tbody>
            </table>
              <div class="col-md-4 editcont-style">
                  <?php 
                  if(!empty($getDeleted_date)){
                  echo '<button class="btn btn-warning">Your account will have deleted in few days.</button>';
                  }
                  else{
                  echo $this->Form->postLink(__('Delete Account'), ['controller'=>'Users','action' => 'delete'], ['confirm' => __('Are you sure you want to delete # {0}?'),'class'=>'btn btn-danger']);
                  }
                  ?>
              </div>
          </div>
          <!-- /.table-responsive -->
    </div>
       <div class="col-md-6">
          <h4 class="box-title">Access Settings</h4>
           <?= $this->Form->create(null,['url'=>['controller'=>'users','action'=>'updatepassword']]) ?>
                <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('password',['class'=>'form-control','placeholder'=>'Password'])?>
                    </div>
                  </div>
                </div>

            <?= $this->Form->button(__('Save'),['class'=>'btn btn-danger']) ?>
            <?= $this->Form->end() ?>
        </div>

          <div class="col-md-6">
            <h4 class="box-title">Set My Subscription Price</h4>
             <?= 'Current Price: '.$this->Number->currency($user->amount,"USD").'/Month'?> 
             <?= $this->Form->create(null,['url'=>['controller'=>'users','action'=>'upateamount']]) ?>
             <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('amount',['label'=>'Per Month','class'=>'form-control','placeholder'=>'Amount'])?>
                    </div>
                  </div>
              </div>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-danger']) ?>
            <?= $this->Form->end() ?>

          </div>

          <div class="col-md-6">
            <h4 class="box-title">Payment Setting</h4>
            <?= $this->Form->create(null,['url'=>['controller'=>'payment-settings','action'=>'add']]) ?>
             <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('credit_card_type',['options'=>['Visa'=>'Visa','Mastercard'=>'Mastercard'],'class'=>'form-control','placeholder'=>'credit Card Type','default'=>$paymentSetting->credit_card_type])?>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('credit_card_number',['class'=>'form-control','placeholder'=>'credit Card Number','value'=>$paymentSetting->credit_card_number])?>
                    </div>
                  </div>
              </div>
               <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('expiration_date',['class'=>'form-control','placeholder'=>'Security Date','value'=>$paymentSetting->expiration_date])?>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('security_code',['class'=>'form-control','placeholder'=>'Security code','value'=>$paymentSetting->security_code])?>
                    </div>
                  </div>
              </div>

            <?= $this->Form->button(__('Save'),['class'=>'btn btn-danger']) ?>
            <?= $this->Form->end() ?>
          </div><!-- Payment Setting -->

          <?php if($this->request->session()->read('Auth.Users.role_id')==2):?>
          <div class="col-md-6">
            <h4 class="box-title">Bank /Paypal/Diret Deposit </h4>
             <?= $this->Form->create(null,['url'=>['controller'=>'banks','action'=>'add']]) ?>
             <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('bank_name',['class'=>'form-control','placeholder'=>'Bank Name','value'=>$bank->bank_name])?>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('bank_account_number',['class'=>'form-control','placeholder'=>'Bank Account Number','value'=>$bank->bank_account_number])?>
                    </div>
                  </div>
              </div>
               <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('paypal_account',['class'=>'form-control','placeholder'=>'Paypal Account','value'=>$bank->paypal_account])?>
                    </div>
                  </div>
              </div>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-danger']) ?>
            <?= $this->Form->end() ?>
          </div><!-- Bank Paypal -->
          <?php endif;//end if ?>

           <div class="col-md-6">
            <h4 class="box-title">Subscription Setting</h4>
             <?php 
             if($followings->count()>0){
              $i=($this->Paginator->current()*10+1)-10; 
                foreach ($followings as $follower) {
                  echo "<p>".$follower->user->full_name." , subscribed since ".date("Y-m-d",strtotime($follower->created)).", ".$this->Number->currency($follower->user->amount,"USD")." <a href='".$this->request->webroot.$follower->user->username."' class='btn btn-danger'>View Profile</a></p>";
                }
             }//end following
             else{
              echo '<p> Subscription not available. </p>';
             }
             ?>
          </div><!-- Subscription Setting -->

          <div class="col-md-6"><!-- Keywords -->
            <h4 class="box-title">Model Attribute</h4>
            <?php echo $this->Form->create(null,['url'=>['controller'=>'Users','action'=>'updatekeywords']]);

             if($role_id == 2)
             {  ?>
              <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
<?php echo  $this->Form->control('mykeywords',['label'=>'Model Attributes','class'=>'form-control','placeholder'=>'My Keywords','value'=>$user->mykeywords]); ?>
                    </div>
                  </div>
              </div>

              <?php
              } ?>
               <div class="form-group">
                  <div class="cols-sm-10">
                    <div class="input-group">
                    <?= $this->Form->control('intrestedkeywords',['label'=>'Interested Model','class'=>'form-control','placeholder'=>'Keywords I`m interested in','value'=>$user->intrestedkeywords])?>
                    </div>
                  </div>
              </div>
           
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-danger']) ?>
            <?= $this->Form->end() ?>
          </div><!-- Keywords -->

        </div>
</div>
<script type="text/javascript">
  function ontriger(togglename){
    var setUrl="<?php echo $this->Url->build(['controller'=>'Users','action'=>'togglenotification']); ?>";
   $.ajax({
      url:setUrl,dataType:'json',data:{name:togglename},method:'post', success: function(result){
                    
             }//end success
        });
  }
</script>