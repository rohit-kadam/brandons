
<div class="userMedias index large-9 medium-8 columns content">
    <h3><?= __('Model List') ?></h3>
        <div class="row">
            <?php foreach ($models as $model): //pr($model)?>
                <div class="col-md-3">
                <?= $this->Html->image('profile/'.$model->user_medias[0]['body_pics'],['class'=>'img-thumbnail','style'=>'width:200px; height:200px;']) ?>
                <?= $this->Html->link('Follow',['controller'=>'Followers','action'=>'follow', $model->id],['class'=>'btn btn-primary'])?>
                </div>    
            <?php endforeach; ?>
        </div>    
   <?= $this->Awesome->getPagination() ?>
</div>
