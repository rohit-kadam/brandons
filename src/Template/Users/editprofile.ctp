<?php 
      echo $this->Html->css(array('crop/cropper.min','crop/main')); // for css
        echo $this->Html->script(array(/*'crop/bootstrap.min',*/'crop/cropper.min','crop/main')); // for scrolling on mouse 
     ?>   
<div class="container">
    <div class="col-md-12 col-xs-12 col-sm-12 box box-danger topspace">
      <div class="box-body">
        <div class="col-md-3 edit-profile" style="margin-top: 15px">
            <!-- FOR CROPPING IMAGE FUNCTIONALITY HERE -->         
            <div id="crop-avatar" class="custom-crop-avatar1">
                <!-- Current avatar -->
                <div class="avatar-view">
                          <span class="fa fa-pencil edit-bg" aria-hidden="true"></span>
                         <?php

                          $profile_image= (!empty($user['user_medias'][0]['face_pics'])?$user['user_medias'][0]['face_pics']:"default-pics.png");
                          echo $this->Html->image('profile/'.$profile_image, ['class'=>'profile img-circle user-profileimg','alt' => 'User Image','width'=>200]);  
                             ?>
                </div>
                    <!-- Cropping modal -->
                <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                   <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <form class="avatar-form" action="<?php echo $this->request->webroot;?>updateprofiles/getrequest" enctype="multipart/form-data" method="post">  
                          <!--  <form class="avatar-form" action="#" enctype="multipart/form-data" method="post"> -->
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="avatar-modal-label">Upload profile picture</h4>
                          </div>
                          <div class="modal-body">
                                  <div class="avatar-body">
                                    <!-- Upload image and data -->
                                          <div class="avatar-upload">
                                                <input type="hidden" class="avatar-src" name="avatar_src">
                                                <input type="hidden" class="avatar-data" name="avatar_data">
                                                <label for="avatarInput">Local upload</label>
                                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                          </div>
                                          <!-- Crop and preview -->
                                          <div class="row">
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                              <div class="avatar-wrapper"></div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="avatar-preview preview-lg"></div>
                                              <div class="avatar-preview preview-md"></div>
                                              <div class="avatar-preview preview-sm"></div>
                                            </div>
                                          </div>
                                          <div class="row avatar-btns">
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                              <button type="submit" class="btn btn-primary btn-block avatar-save">Upload</button>
                                            </div>
                                          </div>
                                  </div>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /.modal -->
            </div>
        </div>     
        <div class="col-md-9 editcont-style">
            <?= $this->Form->create($user,['enctype'=>'multipart/form-data']) ?>
            <fieldset>
                <legend><?= __('Edit Profile') ?></legend>
                <?php
                    echo $this->Form->control('firstname',['label'=>'First Name','class'=>'form-control inputedit-cust']);
                    echo $this->Form->control('lastname',['label'=>'Last Name','class'=>'form-control inputedit-cust']);
                    echo $this->Form->control('username',['label'=>'Username','class'=>'form-control inputedit-cust']);
                    echo $this->Form->control('email',['class'=>'form-control inputedit-cust']);
                    echo $this->Form->control('phone',['class'=>'form-control inputedit-cust']);
                    echo $this->Form->control('datebirth',['minYear' => date('Y')-35,'maxYear' => date('Y')-18,'placeholder'=>'Birthdate','class'=>'form-control inputedit-cust']);
                    echo $this->Form->control('aboutus',['label'=>'About Us','class'=>'form-control inputedit-cust']);

                    if($role_id==2){
                        echo $this->Form->control('website',['class'=>'form-control']);
                        echo $this->Form->control('address',['class'=>'form-control']);
                        echo $this->Form->control('amount',['class'=>'form-control']);    
                        echo $this->Form->control('pics',['type'=>'file','class'=>'form-control']);
                        echo $this->Form->control('body_pics',['type'=>'file','class'=>'form-control']);
                        echo $this->Form->control('videos',['type'=>'file','class'=>'form-control']);
                        echo $this->Form->control('id_proof',['type'=>'file','class'=>'form-control']);
                        echo $this->Form->control('model_attribute',['class'=>'form-control']);
                    }
                    
                ?>
            </fieldset>
            <?= $this->Form->button(__('update'),['class'=>'btn btn-danger pull-right btn-topspace']) ?>
            <?= $this->Form->end() ?>
        </div>
      </div>
    </div>
</div>

<script>
    // start date
    $("select[name='datebirth[year]']").addClass('form-control start_date_y');
    $("select[name='datebirth[month]']").addClass('form-control start_date_m');
    $("select[name='datebirth[day]']").addClass('form-control start_date_m');
</script>