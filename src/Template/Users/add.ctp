<?= $this->Flash->render() ?>
<div class="brandons-outerbox1">

      <div  class="brandons-innerbox">
         <div class="banner-top">

            <div class="panel with-nav-tabs panel-cust">
                <div class="panel-heading panel-heading-cust">
                      <ul class="nav nav-tabs bc-tabs">
                          <li class="active model-litabs"><a href="#tab1danger" data-toggle="tab">Model Sign Up</a></li>
                      </ul>
                </div>
                <div class="panel-body panel-body-cust">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1danger">        
                             <?= $this->Form->create($user,['enctype'=>'multipart/form-data']) ?>
                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                    <?= $this->Form->hidden('role_id',['value'=>2])?>

                                    <?= $this->Form->control('firstname',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'First Name'])?>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('lastname',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Last Name'])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('username',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Username'])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group ptop-11">
                                <div class="cols-sm-10">
                                  <div class="input-group input-birthdate input-group-cust">
                                    <span class="input-group-addon input-gp-btncust bordernone"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('datebirth',['minYear' => date('Y')-35,'maxYear' => date('Y')-18,'class'=>'form-control','label'=>false,'placeholder'=>'Birthdate'])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('email',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Email','id'=>'createemail'])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('phone',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Phone'])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('password',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Password','id'=>'createpassword'])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('confirmpassword',['type'=>'password','class'=>'form-control form-controlinput-cust','placeholder'=>'Confirm Password','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-globe fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('address',['class'=>'form-control form-controlinput-cust','placeholder'=>'Your Address','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-picture-o fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('pics',['type'=>'file','class'=>'form-control form-controlinput-cust padt0','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-address-book-o fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('body_pics',['type'=>'file','class'=>'form-control form-controlinput-cust padt0','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-picture-o fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('face_pics',['type'=>'file','class'=>'form-control form-controlinput-cust padt0','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-file-video-o fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('videos',['type'=>'file','class'=>'form-control form-controlinput-cust padt0','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-id-card-o fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('id_proof',['type'=>'file','class'=>'form-control form-controlinput-cust padt0','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-user-times fa-lg" aria-hidden="true"></i></span>

                                    <?= $this->Form->control('mykeywords',['class'=>'form-control form-controlinput-cust padt0','placeholder'=>'Model best attribute tag','label'=>false])?>
                                  </div>
                                </div>
                              </div>
                              <?= $this->Form->button(__('Sign up'),['class'=>'btn btn-default btn-round btn-login']); ?>

                                <!-- <div class="form-group">
                                  <div class="g-recaptcha" data-theme="dark" data-sitekey="6Lcd-DIUAAAAAKmm--NldhamUv4S6_D3W1aJTzad" ></div>
                                </div> -->
                              <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
$("select").addClass('datebirth selectpicker');

$(document).ready(function() {
    $('#myCarousel').carousel({
      interval: 10000
  })
});
</script>
