
<div class="container">
	<div class="mprofile-box">

<?php if(count($searchinData->all())>0){ ?>
	<?php foreach ($searchinData->all() as $key => $value) { 

		/*	foreach ($value['subscriptions'] as $subscription_key => $subscription_value) {
				
			}*/
		?>
		<div class="col-md-4">
			<div class="box box-widget widget-user">
	            <!-- Add the bg color to the header using any of the bg-* classes -->
	            <div class="widget-user-header bg-aqua-active">
	              <h3 class="widget-user-username">		
	              
					<?= $this->Html->link(ucwords($value->fullname),['controller'=>'Followers','action'=>'follow',$value->id]);
						?></h3>
	              <h5 class="widget-user-desc">@<?= $value->username?></h5>
	            </div>
	            <div class="widget-user-image">
	              <?php echo $this->Html->image('profile/'.$value->user_medias[0]['face_pics'],['class'=>'fx-pimg img-responsive img-circle direct-recommend']); ?>
	            </div>
	            <div class="box-footer">
	              <div class="row">
	                <div class="col-sm-4 border-right">
	                  <div class="description-block">
	                    <h5 class="description-header">Subscription</h5>
	                    <span class="description-text"><?= $this->Number->currency($value->amount) ?></span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4 border-right">
	                  <div class="description-block">
	                    <h5 class="description-header">Date</h5>
	                    <span class="description-text"><?= date("Y-m-d H:i:s",strtotime($value->created))?></span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4">
	                  <div class="description-block">
	                    <h5 class="description-header">Auto renewal</h5>
	                    <span class="description-text">ON</span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	               
	              </div>
	            
	            </div>
	            <div class="cancel-subscription text-center">
 <?php echo $this->Html->link('Follow '.$this->Number->currency($value->amount).' (Per Month)',['controller'=>'Subscriptions','action'=>'add',$value->id],['class'=>'btn btn-danger']) ?>

	          

	            </div>
	        </div>
        </div>
       
<?php		}	
			}
			else{
			 		echo '<div class="alert alert-danger"><h4> Followings memeber are not available.</h4></div>';
			 	} ?>
<div class="col-md-4">
        </div>        
        <div class="col-md-4">
        </div>
	</div>
</div>
