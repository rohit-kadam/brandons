<div class="container">
    <div class="row">
        <div class="box box-default box-top-sm">
            <div class="box-header with-header box-header-cust">
                <h3 class="inline-block"><?= __('Users') ?></h3>
                <div class="row m-r-10">
                    <div class="col-md-3">
                          <div class="info-box">
                              <span class="info-box-icon bg-red">
                                <i class="fa fa-users" aria-hidden="true"></i>
                              </span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Registered Model/Member</span>
                                <span class="info-box-number"> <?php echo $total_register_member; ?></span>
                              </div>
                          </div>
                    </div>
                    <div class="col-md-2 nopad-both">
                          <div class="info-box">
                              <span class="info-box-icon bg-red">
                                <i class="fa fa-user" aria-hidden="true"></i>
                              </span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Members</span>
                                <span class="info-box-number"><?php echo $usercount; ?></span>
                              </div>
                          </div>
                    </div>
                    <div class="col-md-2 nopad-right">
                          <div class="info-box">
                              <span class="info-box-icon bg-red">
                                <i class="fa fa-user" aria-hidden="true"></i>
                              </span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Models</span>
                                <span class="info-box-number"><?php echo $modelcount; ?></span>
                              </div>
                          </div>
                    </div>
                    <div class="col-md-3">
                          <div class="info-box">
                              <span class="info-box-icon bg-red">
                                <i class="fa fa-user" aria-hidden="true"></i>
                              </span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Active Models</span>
                                <span class="info-box-number"><?php echo $Active_models; ?></span>
                              </div>
                          </div>
                    </div>
                    <div class="col-md-2 nopad-left">
                          <div class="info-box">
                              <span class="info-box-icon bg-red">
                                <i class="fa fa-user-times" aria-hidden="true"></i>
                              </span>
                              <div class="info-box-content">
                                <span class="info-box-text">Total Active Members</span>
                                <span class="info-box-number"><?php echo $Active_Users; ?></span>
                              </div>
                          </div>
                    </div>
                </div>

                <div class="row">
                      <div class="col-md-4">
                         <?php 
                          echo $this->Form->create(null, ['url' => ['controller' => 'users', 'action' => 'globalnotification'] ]); ?>
                        <div class="form-group">
                          <div class="col-md-12 col-xs-12 nopad-both">
                            <label for="email">Send Notification to All User:</label>
                          </div>
                          <div class="col-md-8  nopad-both">
                            <?php   echo $this->Form->input('message',array('rows'=>'1','type'=>'textarea','label'=>false,'class'=>'form-control b-rad0', 'placeholder'=>'Enter Message','required'=>true));?>
                            </div>
                        </div>
                        <?php echo $this->Form->button(__('Send'),['class'=>'btn btn-danger btn-spaces']);?>
                          <?php echo $this->Form->end(); ?>
                     </div>
                     <div class="col-md-8 top30">
                          <?php
                      echo $this->Html->link('Manage Top Models',['controller'=>'FeatureModels','action'=>'index'],['class'=>'btn 
                      btn-danger pull-right']) ?>
                     </div>
                </div><!--row-->
               
            </div>
            <div class="box-body">
                <table class="table table-responsive">
                      <thead>
                                <tr>
                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('Fullname') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>

                                    <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                      <tbody>   
                                <?php $i=($this->Paginator->current()*10+1)-10; ?>    
                                <?php foreach ($users as $user): ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= h($user->fullname) ?></td>
                                    <td><?= h($user->role->name) ?></td>
                                    <td><?= h($user->email) ?></td>
                                    <td><?= h($user->status) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                </table>
                <div class="m-r-pagination">
                  <?= $this->Awesome->getPagination() ?>
                </div>
            </div>
        </div>
    </div>
</div>




