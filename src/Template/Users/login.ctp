<?php $this->assign('title', 'Login');?>
<div class="alert-outerdiv"><?= $this->Flash->render() ?></div>

<div class="brandons-outerbox">
      <div  class="brandons-innerbox">
         <div class="banner-top">
            <div class="ban-out-tbox">
                <?= $this->Html->image('formtop.png',['class'=>'img-responsive imgban-cust'])?>
                <?= $this->Html->image('bc-logo.png',['class'=>'img-responsive imgbc-logo'])?>
            </div>
            <div class="panel with-nav-tabs panel-cust">
                <div class="panel-heading panel-heading-cust">
                      <ul class="nav nav-tabs bc-tabs">
                          <li class="active"><a href="#tab1danger" data-toggle="tab">Log in</a></li>
                          <li><a href="#tab2danger" data-toggle="tab">Sign Up</a></li>
                      </ul>
                </div>
                <div class="panel-body panel-body-cust">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1danger">        
                             <?= $this->Form->create() ?>
                              <div class="form-group mr-btm-30">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('email',['class'=>'form-control form-controlinput-cust','placeholder'=>'Email Id','label'=>false])?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group btm-mr-md input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-lock fa-lg fa-lock-cust" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('password',['class'=>'form-control form-controlinput-cust','placeholder'=>'Password','label'=>false])?>
                                    <?= $this->Form->hidden('login',['value'=>1]) ?>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                  <div class="g-recaptcha" data-theme="dark" data-sitekey="6Lcd-DIUAAAAAKmm--NldhamUv4S6_D3W1aJTzad" ></div>
                                </div>
                                
                              <?= $this->Form->button(__('Log in'),['class'=>'btn btn-default btn-round btn-login1']); ?>

                                
                              <?= $this->Form->end() ?>
                                <?= $this->Html->link('Forgot password',['controller'=>'Users','action'=>'forgotpassword'],['class'=>'forgot-text-style pull-right',])?>
                                <div class="clearfix"></div>
                                <h6 class="background-line text-center"><span>Or</span></h6>
                                <?= $this->Form->button(__('<i class="fa fa-twitter" aria-hidden="true"></i>
Login With Twitter'),array('class' => 'btn btn-default btn-round btn-login','div' => false)); ?>
                        </div>
                        <div class="tab-pane fade" id="tab2danger">                            
                            <?= $this->Form->create($user,['id'=>'signupForm']) ?>
                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-users fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('firstname',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'First Name'])?>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-users fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('lastname',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Last Name'])?>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-users fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('username',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Username'])?>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('email',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Email','id'=>'createemail'])?>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('phone',['class'=>'form-control form-controlinput-cust','placeholder'=>'Phone','label'=>false])?>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('password',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Password','id'=>'createpassword'])?>
                                        
                                      </div>
                                    </div>
                                  </div>                                  
                                  <div class="form-group">
                                    <div class="cols-sm-10">
                                      <div class="input-group input-group-cust">
                                        <span class="input-group-addon input-gp-btncust"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <?= $this->Form->control('confirmpassword',['type'=>'password','class'=>'form-control form-controlinput-cust','placeholder'=>'Confirm password','label'=>false])?>
                                      </div>
                                    </div>
                                  </div>
                                  <?= $this->Form->button(__('Sign up'),['class'=>'btn btn-default btn-round btn-login']); ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <?= $this->Html->link(__('APPLY TO BE A MODEL'),array('controller'=>'Users','action'=>'add'),array('class' => 'btn btn-default btn-login-model','div' => false)); ?>
            </div>
        </div>
      </div>
</div>
<div class="container">
    <div class="col-md-12 col-xs-12 col-sm-12 well well-cust">
        <div class="thumbnail-slider">
            <h4 class="modeltop-text text-center">Top Models</h4>

              <ul id="flexiselDemo3">
              <?php  foreach ($featureModels as $key => $user){?>
                <li><?= $this->Html->image('profile/'.$user->user_medias[0]['face_pics'],['class'=>'img-responsive imgban-cust'])?>
                    <div class="overlay-box">
                      <div class="text-center model-head-text"><?= (strlen($user->fullname)>16)?substr(ucwords($user->fullname),0,15).'...':ucwords($user->fullname) ?></div>
                      <div class="text-center model-subhead-text"><?= (array_key_exists($user->id, $subscribedUsers)?$subscribedUsers[$user->id]:0 )?> followers</div>
                    </div>
                  </li>
              <?php } ?>                                             
              </ul> 

            <!--/myCarousel-->
        </div>
        <!--/well-->
    </div>
</div>
<script type="text/javascript">
$("#signupForm").submit(function(e){

e.preventDefault();	

$(".error-message").remove();
	
    var setUrl="<?php echo $this->Url->build(['controller'=>'Users','action'=>'login']); ?>";
            $.ajax({
              url:setUrl,dataType:'json',data:$("#signupForm").serialize(),method:'post', success: function(result){
              			//alert(result['result']);
	              		if(result['result']=="fail"){
	              			$.each(result.data, function(model, errors) {
                        
		                        for (fieldName in this) {
		                          //alert(model+' err '+this[fieldName]);
		                          if(model=="email"){
		                          	var element = $("#createemail");
		                          }
		                          else if(model=="password"){
		                          	var element = $("#createpassword");
		                          }
		                          else{
		                          	var element = $("#" + model);
		                          } 
		                            var create = $(document.createElement('div')).insertAfter(element);
		                            create.addClass('error-message').text(this[fieldName])
		                        }
		                    });		
		                }
		                else{
		                	location.reload();
		                }
		                
                    }
                }); 

            		return false;
          });

</script>
<script>
$(window).load(function() {
    
    $("#flexiselDemo3").flexisel({
        visibleItems: 5,
        itemsToScroll: 1,         
        autoPlay: {
            enable: true,
            interval: 5000,
            pauseOnHover: true
        }        
    });
    
    $("#flexiselDemo4").flexisel({
        infinite: false     
    });    
    
});
</script>