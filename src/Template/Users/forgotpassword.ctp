<?php $this->assign('title', 'Forgot Password');?>

<?= $this->Flash->render() ?>
<div class="brandons-outerbox1">

      <div  class="brandons-innerbox">
         <div class="banner-top">

            <div class="panel with-nav-tabs panel-cust">
                <div class="panel-heading panel-heading-cust">
                      <ul class="nav nav-tabs bc-tabs">
                          <li class="active model-litabs"><a href="#tab1danger" data-toggle="tab">Forgot Password</a></li>
                      </ul>
                </div>
                <div class="panel-body panel-body-cust">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1danger">        
                             <?= $this->Form->create() ?>
                           
                              <div class="form-group">
                                <div class="cols-sm-10">
                                  <div class="input-group input-group-cust">
                                    <span class="input-group-addon input-gp-btncust"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                    <?= $this->Form->control('email',['class'=>'form-control form-controlinput-cust','label'=>false,'placeholder'=>'Email','id'=>'createemail'])?>
                                  </div>
                                </div>
                              </div>

                            
                              <?= $this->Form->button(__('Submit'),['class'=>'btn btn-default btn-round btn-login']); ?>

                              <?= $this->Form->end() ?>
                              <a href="<?php echo $this->Url->build(['controller'=>'Users','action'=>'login']) ?>">Back to Login</a> <br>
                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>