<style>body{background:#e9ebee;}  .quick-contact {
  position: fixed;
  right: 20px;
  bottom: 0px;
  z-index: 100;min-width:225px;}
  .btn-quick {
  padding: 8px 18px;
  color: #fff;
  font-size: 16px;
  font-weight: 700;
  width: 100%;
  background: #d9534f !important;
  border-radius: 8px 8px 0 0;
  border: #d9534f;}  
  .btn-quick1 {
  height: 34px;
  padding: 5px 0px 5px 18px;
  color: #fff;
  font-size: 16px;
  font-weight: 700;
  width: 100%;
  background: #d9534f !important;
  border-radius: 8px 8px 0 0;
  border: #d9534f;} 
  .btn-quick:active {
  border-radius: 8px 8px 0 0;}  
  .btn-quick:focus {
  border-radius: 8px 8px 0 0;}  
  .inner-well {
  background: #fff;
  padding: 5px 10px !important;
  border-radius: 0px !important;
  border:1px solid #ccc;
  }
  .online{margin-top:7px;width:16px;height:16px;background:green;border-radius:50%;display:inline-block;float:left;}
  img.profile-photo-sm1 {height: 40px; width: 40px; border-radius: 50%; margin-right: 15px; margin-top: -5px; margin-left: 24px; } 
  .right-panel-head1 {margin: 6px 0px 3px;}
  .right-panel-head1 {position: relative;top: 6px;}
</style>
<div class="container">
  <div class="content-wrapper">
      <div class="row">
          <div class="col-md-9">
            <div class="create-post">
                <div class="row">
                  <div class="text-right mrbtm-10">
                  <?= $this->Html->link('My Feed(Edit)',['controller'=>'users','action'=>'myfeed'],['class'=>'btn btn-danger']); ?>
                  <?= $this->Html->link('Normal Feed',['controller'=>'users','action'=>'normalfeed'],['class'=>'btn btn-danger']); ?>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 nopad-both post-mbox">
                      <div  id="new_status">
                        <?= $this->Form->create($post,['enctype'=>'multipart/form-data','id'=>'createpostForms']) ?>

                        <div class="col-md-7 col-sm-7" id="post_content">
                            <?= $this->Html->image('profile/'.$model->user_medias[0]['face_pics'],['class'=>'profile-photo img-responsive profile-photo-md'])?>
                              <div class="textarea_wrap">
                                <?php echo $this->Form->control('content',['type'=>'textarea','class'=>'form-control textarea-cust1','rows'=>2,'placeholder'=>'What`s on your mind?','label'=>false,'id'=>'content']); ?>
                              </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                          <ul class="navbar-nav col-xs-12" id="post_header" role="navigation">
                            <li><a href="#" id="clearfiles"><span class="fa fa-pencil-square-o"></span></a></li>
                            <li><a href="#" id="get_file"><span class="fa fa-file-image-o"></span></a>
                            <?php echo $this->Form->control('photos',['type'=>'file','label'=>false,'id'=>'my_file']); ?>
                            </li>
                            <li><a href="#" id="get_file1"><span class="fa fa-video-camera"></span></a>
                            <?php echo $this->Form->control('videos',['type'=>'file','label'=>false,'id'=>'my_file1']); ?>
                            </li>
                            <li class="pull-right btnglass-style">
                               <?= $this->Form->button(__('Post'),['class'=>'btn glass']) ?>
                            </li>
                          </ul>
                        </div>
                         <?= $this->Form->end() ?>
                      </div>
                    <div class="tools pull-right">
                    </div>
                  </div>
                </div>
            </div>
             <!-- Post data--> 
             <?php //pr($posts); exit;?>
            <?php echo $this->Awesome->getPostHtml($posts) ?>
          </div>
            <?php echo $this->element('rightpanel') ?>
      </div>
  </div>
    <?= $this->Awesome->getChats($model); ?>
</div>
<script>
// createpostForms
$("#createpostForms").submit(function(e){

e.preventDefault(); 

$(".error-message").remove();
    var formData = new FormData($(this)[0]);
    var setUrl="<?php echo $this->Url->build(['controller'=>'Posts','action'=>'add']); ?>";
            $.ajax({
              url:setUrl,async: false,cache: false,contentType: false,processData: false,dataType:'json',data:formData,method:'post', success: function(result){
                    //alert(result['result']);
                    if(result['result']=="fail"){
                      $.each(result.data, function(model, errors) {
                        
                            for (fieldName in this) {
                              //alert(model+' err '+this[fieldName]);
                              if(model=="email"){
                                var element = $("#content");
                              }
                              else{
                                var element = $("#" + model);
                              }
                                
                                var create = $(document.createElement('div')).insertAfter(element);
                                create.addClass('error-message').text(this[fieldName])
                            }
                        });   
                    }
                    else{
                      location.reload();
                    }
                    
                    }
                }); 

                return false;
          });

  document.getElementById('get_file').onclick = function() {
      document.getElementById('my_file').click();
      $("#my_file1").val('');
  };  
  document.getElementById('get_file1').onclick = function() {
      document.getElementById('my_file1').click();
      $("#my_file").val('');
  };

  document.getElementById('clearfiles').onclick= function(){
    $("#my_file1").val('');
    $("#my_file").val('');
  };  

  $(".user-online").click(function(){
      //alert("hi");
      $(".quick-contact1").show();
  });  
  $(".btn-box-tool").click(function(){
      //alert("hi");
      $(".quick-contact1").hide();
  });


$('.input-form-cmn1').keypress(function(event) {
  if (event.which == 13) {
    event.preventDefault();
      var s = $(this).val();
      $(this).val(s+"\n");
  }
});
</script>