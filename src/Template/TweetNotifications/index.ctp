<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TweetNotification[]|\Cake\Collection\CollectionInterface $tweetNotifications
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tweet Notification'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tweetNotifications index large-9 medium-8 columns content">
    <h3><?= __('Tweet Notifications') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subsriber_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tweetNotifications as $tweetNotification): ?>
            <tr>
                <td><?= $this->Number->format($tweetNotification->id) ?></td>
                <td><?= $tweetNotification->has('user') ? $this->Html->link($tweetNotification->user->id, ['controller' => 'Users', 'action' => 'view', $tweetNotification->user->id]) : '' ?></td>
                <td><?= h($tweetNotification->subsriber_type) ?></td>
                <td><?= h($tweetNotification->status) ?></td>
                <td><?= h($tweetNotification->created) ?></td>
                <td><?= h($tweetNotification->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tweetNotification->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tweetNotification->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tweetNotification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tweetNotification->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
