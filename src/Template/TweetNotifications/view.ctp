<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TweetNotification $tweetNotification
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tweet Notification'), ['action' => 'edit', $tweetNotification->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tweet Notification'), ['action' => 'delete', $tweetNotification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tweetNotification->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tweet Notifications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tweet Notification'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tweetNotifications view large-9 medium-8 columns content">
    <h3><?= h($tweetNotification->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $tweetNotification->has('user') ? $this->Html->link($tweetNotification->user->id, ['controller' => 'Users', 'action' => 'view', $tweetNotification->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subsriber Type') ?></th>
            <td><?= h($tweetNotification->subsriber_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($tweetNotification->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tweetNotification->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($tweetNotification->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($tweetNotification->modified) ?></td>
        </tr>
    </table>
</div>
