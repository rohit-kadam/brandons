<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EmailNotification[]|\Cake\Collection\CollectionInterface $emailNotifications
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Email Notification'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="emailNotifications index large-9 medium-8 columns content">
    <h3><?= __('Email Notifications') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subsriber_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($emailNotifications as $emailNotification): ?>
            <tr>
                <td><?= $this->Number->format($emailNotification->id) ?></td>
                <td><?= $emailNotification->has('user') ? $this->Html->link($emailNotification->user->id, ['controller' => 'Users', 'action' => 'view', $emailNotification->user->id]) : '' ?></td>
                <td><?= h($emailNotification->subsriber_type) ?></td>
                <td><?= h($emailNotification->status) ?></td>
                <td><?= h($emailNotification->created) ?></td>
                <td><?= h($emailNotification->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $emailNotification->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $emailNotification->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $emailNotification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailNotification->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
