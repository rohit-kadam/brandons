<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EmailNotification $emailNotification
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Email Notification'), ['action' => 'edit', $emailNotification->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Email Notification'), ['action' => 'delete', $emailNotification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailNotification->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Email Notifications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Email Notification'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="emailNotifications view large-9 medium-8 columns content">
    <h3><?= h($emailNotification->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $emailNotification->has('user') ? $this->Html->link($emailNotification->user->id, ['controller' => 'Users', 'action' => 'view', $emailNotification->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subsriber Type') ?></th>
            <td><?= h($emailNotification->subsriber_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($emailNotification->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($emailNotification->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($emailNotification->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($emailNotification->modified) ?></td>
        </tr>
    </table>
</div>
