<div class="container">
	<div class="mprofile-box">

	 <?php 
 	if($fans->count()>0){
 	foreach ($fans as $key => $value) { ?>

		<div class="col-md-4">
			<div class="box box-widget widget-user">
	            <!-- Add the bg color to the header using any of the bg-* classes -->
	            <div class="widget-user-header bg-aqua-active">
	              <h3 class="widget-user-username"><?= $this->Html->link(ucwords($value->user->fullname),['controller'=>'Followers','action'=>'follow',$value->user->id]) ?></h3>
	              <h5 class="widget-user-desc"><?= $value->user->email?></h5>
	            </div>
	            <div class="widget-user-image">
	              <?php echo $this->Html->image('profile/'.$value->user->user_medias[0]['face_pics'],['class'=>'fx-pimg img-responsive img-circle direct-recommend']); ?>
	            </div>
	            <div class="box-footer">
	              <div class="row">
	                <div class="col-sm-4 border-right">
	                  <div class="description-block">
	                    <h5 class="description-header">Subscription</h5>
	                    <span class="description-text"><?= $this->Number->currency($value->user->amount)?></span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4 border-right">
	                  <div class="description-block">
	                    <h5 class="description-header">Date</h5>
	                    <span class="description-text"><?= date("Y-m-d H:i:s",strtotime($value->user->created))?></span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4">
	                  <div class="description-block">
	                    <h5 class="description-header">Auto renewal</h5>
	                    <span class="description-text">ON</span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	              </div>
	              <!-- /.row -->
	            </div>
	            <div class="cancel-subscription text-center">
	            	<?php if(in_array($value->user->id,$follower_idArr)){ ?>
	            		<?= $this->Form->postLink($this->Html->tag("i", "", array("class" => "fa fa-unlock-alt")).' Cancel subscription', ["action" => "delete", $value->user->id], ["escape" => false,"confirm" => __("Are you sure you want to unsubscribe {0}?", ucfirst($value->user->id))]) ?>
	            	<?php }
	            	else{ ?>
	            		<a href="<?= $this->Url->build(['controller'=>'Subscriptions','action'=>'add',$value->user->id])?>" class="btn btn-default"><i class="fa fa-unlock-alt"></i> Follow</a>

	            	<?php	}?>
	            	
	            </div>
	        </div>
        </div>

<?php }
 	}
 	else{
 		echo '<div class="alert alert-danger"><h4> Followers memeber are not available.</h4></div>';
 	}
 ?>

		
        <div class="col-md-4">
        </div>        
        <div class="col-md-4">
        </div>
	</div>
</div>


