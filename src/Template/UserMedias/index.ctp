<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMedia[]|\Cake\Collection\CollectionInterface $userMedias
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Media'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userMedias index large-9 medium-8 columns content">
    <h3><?= __('User Medias') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_proof') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pics') ?></th>
                <th scope="col"><?= $this->Paginator->sort('body_pics') ?></th>
                <th scope="col"><?= $this->Paginator->sort('face_pics') ?></th>
                <th scope="col"><?= $this->Paginator->sort('videos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('model_attribute') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userMedias as $userMedia): ?>
            <tr>
                <td><?= $this->Number->format($userMedia->id) ?></td>
                <td><?= $userMedia->has('user') ? $this->Html->link($userMedia->user->id, ['controller' => 'Users', 'action' => 'view', $userMedia->user->id]) : '' ?></td>
                <td><?= h($userMedia->id_proof) ?></td>
                <td><?= h($userMedia->pics) ?></td>
                <td><?= h($userMedia->body_pics) ?></td>
                <td><?= h($userMedia->face_pics) ?></td>
                <td><?= h($userMedia->videos) ?></td>
                <td><?= h($userMedia->model_attribute) ?></td>
                <td><?= h($userMedia->status) ?></td>
                <td><?= h($userMedia->created) ?></td>
                <td><?= h($userMedia->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userMedia->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userMedia->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userMedia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMedia->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
