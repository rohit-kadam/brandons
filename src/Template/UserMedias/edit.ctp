<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userMedia->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userMedia->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Medias'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userMedias form large-9 medium-8 columns content">
    <?= $this->Form->create($userMedia) ?>
    <fieldset>
        <legend><?= __('Edit User Media') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('id_proof');
            echo $this->Form->control('pics');
            echo $this->Form->control('body_pics');
            echo $this->Form->control('face_pics');
            echo $this->Form->control('videos');
            echo $this->Form->control('model_attribute');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
