<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserMedia $userMedia
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Media'), ['action' => 'edit', $userMedia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Media'), ['action' => 'delete', $userMedia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userMedia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Medias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Media'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userMedias view large-9 medium-8 columns content">
    <h3><?= h($userMedia->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $userMedia->has('user') ? $this->Html->link($userMedia->user->id, ['controller' => 'Users', 'action' => 'view', $userMedia->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Proof') ?></th>
            <td><?= h($userMedia->id_proof) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pics') ?></th>
            <td><?= h($userMedia->pics) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Body Pics') ?></th>
            <td><?= h($userMedia->body_pics) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Face Pics') ?></th>
            <td><?= h($userMedia->face_pics) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Videos') ?></th>
            <td><?= h($userMedia->videos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Model Attribute') ?></th>
            <td><?= h($userMedia->model_attribute) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($userMedia->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userMedia->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userMedia->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userMedia->modified) ?></td>
        </tr>
    </table>
</div>
