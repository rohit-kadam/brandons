<style>body{background:#e9ebee;}  .quick-contact {
  position: fixed;
  right: 20px;
  bottom: 0px;
  z-index: 100;min-width:225px;}
  .btn-quick {
  padding: 8px 18px;
  color: #fff;
  font-size: 16px;
  font-weight: 700;
  width: 100%;
  background: #d9534f !important;
  border-radius: 8px 8px 0 0;
  border: #d9534f;}  
  .btn-quick1 {
  height: 34px;
  padding: 5px 0px 5px 18px;
  color: #fff;
  font-size: 16px;
  font-weight: 700;
  width: 100%;
  background: #d9534f !important;
  border-radius: 8px 8px 0 0;
  border: #d9534f;} 
  .btn-quick:active {
  border-radius: 8px 8px 0 0;}  
  .btn-quick:focus {
  border-radius: 8px 8px 0 0;}  
  .inner-well {
  background: #fff;
  padding: 5px 10px !important;
  border-radius: 0px !important;
  border:1px solid #ccc;
  }
  .online{margin-top:7px;width:16px;height:16px;background:green;border-radius:50%;display:inline-block;float:left;}
  img.profile-photo-sm1 {height: 40px; width: 40px; border-radius: 50%; margin-right: 15px; margin-top: -5px; margin-left: 24px; } 
  .right-panel-head1 {margin: 6px 0px 3px;}
  .right-panel-head1 {position: relative;top: 6px;}
</style>
  <div class="content-wrapper">
      <div class="timeline">
        <div class="timeline-cover">
            <!-- /.box-header -->
            <div class="cover-body cover-pic-box">
              <?= $this->Html->image('profile/'.$model->user_medias[0]['pics'],['class'=>'img-responsive img-cover-user'])?>
              <div class="profie-info">
                <div class="col-md-3">
                <?= $this->Html->image('profile/'.$model->user_medias[0]['face_pics'],['class'=>'profile-photo img-responsive'])?>
                </div>
                <div class="col-md-9">
                  <div class="col-md-3">
                    <div class=" box-gray">
                      <h3 class="ban-headtext"><?= $postsCount?></h3>
                      <p class="btm0 ban-para">Posts</p>
                    </div>
                  </div>                
                  <div class="col-md-3">
                    <div class=" box-gray">
                      <h3 class="ban-headtext"><?= $photosCount?></h3>
                      <p class="btm0 ban-para">Photos</p>
                    </div>
                  </div>               
                  <div class="col-md-3">
                    <div class=" box-gray">
                     <h3 class="ban-headtext"><?= $videosCount?></h3>
                     <p class="btm0 ban-para">Videos</p>
                    </div>
                  </div>                
                  <div class="col-md-3">
                    <div class="box-gray">
                      <h3 class="ban-headtext"><?= $likes ?></h3>
                      <p class="btm0 ban-para">Likes</p>
                    </div>
                  </div>
                  <div class="col-md-12"><div class="f-text">
                  <?php echo $this->Html->link('Follow '.$this->Number->currency($model->amount).' (Per Month)',['controller'=>'Subscriptions','action'=>'add',$model->id],['class'=>'btn btn-danger']);
                   ?></div>
                   <div class="f-text">
                 <?php  echo $this->Html->link('Poke ',['controller'=>'Notifications','action'=>'poke',$model->id],['class'=>'btn btn-danger']);
                   ?></div>

                   </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="container">
          <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-12">

                  <?php
                    if($model->has('user_medias')){
                     // echo $this->Awesome->getPostHtml($posts); 
                      foreach ($model->user_medias as $key => $value) {
                         // $pics = 'http://dev.bitwaretechnologies.com'.$this->request->webroot.'img/profile/'.$value['pics'];
                          $pics = 'img/profile/'.$value['pics'];
                          $body_pics = 'http://dev.bitwaretechnologies.com'.$this->request->webroot.'img/profile/'.$value['body_pics'];
                          $face_pics = 'http://dev.bitwaretechnologies.com'.$this->request->webroot.'img/profile/'.$value['face_pics'];
                          $videos = 'http://dev.bitwaretechnologies.com'.$this->request->webroot.'videos/'.$value['videos'];
                   
                     ?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-4">
                               <img src="<?php echo $body_pics; ?>" class="img-rounded img-responsive" alt="Cinque Terre" > 
                        </div>
                  
                        <div class="col-md-4">
                            <img src="<?php echo $face_pics; ?>" class="auto-style img-rounded img-responsive" alt="Cinque Terre"> 
                        </div>  
                     
                        <div class="col-md-4">
                             <video width="100%" controls>
                                <source src="<?php echo $videos; ?>" type="video/mp4">
                              </video> 
                        </div> 
                    </div>

                     <?php  
                      }
                    }
                    else{
                     echo '<div class="alert alert-danger"><h4> Post are not available.</h4> </div>';    
                    }
                    ?>
                </div>
            </div>
          </div>
        </div>
      </div>     
  </div>

