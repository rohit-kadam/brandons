<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Brandon`s Closet';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!--?= $this->Html->css('base.css') ?-->
    <!--?= $this->Html->css('cake.css') ?-->
    <?= $this->Html->css([
        'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700',
        'https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800',
        'bootstrap/bootstrap.min',
        'bootstrap/login',
        'bootstrap/bootstrap-select',
        'bootstrap/font-awesome.min'])?>
    <?= $this->Html->script(['https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js','bootstrap/bootstrap.min','bootstrap/jquery.flexisel','bootstrap/bootstrap-select','https://www.google.com/recaptcha/api.js'])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?= $this->Flash->render() ?>
    <div class="brandons-wrapper">
    <?= $this->fetch('content') ?>
    </div>
    <footer></footer>
</body>
</html>
