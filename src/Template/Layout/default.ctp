<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Brandon`s Closet';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css([
        'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700',
        'https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800',
        'bootstrap/bootstrap.min',
        'bootstrap/style',
        'bootstrap/bootstrap-select',
        'bootstrap/font-awesome.min'])?>
    <?= $this->Html->script(['bootstrap/jquery-3.2.1.min','bootstrap/bootstrap.min','bootstrap/bootstrap-select','bootstrap/jQuery.verticalCarousel.js','//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js'])?>
    <?= $this->Html->css(['//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css']); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?= $this->Flash->render() ?>
    <?= $this->element('header') ?>
    <div class="container-max">
        <?= $this->fetch('content') ?>
    </div>
    <footer></footer>

    <script>
        $(".verticalCarousel").verticalCarousel({
                currentItem: 1,
                showItems: 2,
        });
            $('ul.main-menu li.dropdown-cust').hover(function() {
              $(this).find('.drop-menu-cmn').stop(true, true).delay('fast').fadeIn('fast');
            }, function() {
              $(this).find('.drop-menu-cmn').stop(true, true).delay('fast').fadeOut('fast');
            });

 //            (function(){
 //                // do some stuff
 //            var getme=setTimeout(function(){ 
 //                     var setUrl="<?php echo $this->Url->build(['controller'=>'Users','action'=>'getnotifications']); ?>";
 //            $.ajax({
 //              url:setUrl,dataType:'json',data:{id:1},method:'post', success: function(result){
 //                        //alert(result['result']);
 //                       $.each(result['data'], function( index, value ) {
 // //                         alert( index + ": " + value );
 //                          toastr.success(value, 'Success Alert', {timeOut: 5000});
 //                        });
 //                     }
 //                });
 //                getme();
 //             }, 10000);

 //            })();

    lognumber = 0;

(function(window, document, undefined) {
    
  var initMyLib = function() {
var setUrl="<?php echo $this->Url->build(['controller'=>'Users','action'=>'getnotifications']); ?>";
            $.ajax({
              url:setUrl,dataType:'json',data:{id:1},method:'post', success: function(result){

                        var opts = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass":"toast-top-right",
                                    "toastClass": "black",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "10000",
                                    "extendedTimeOut": "0",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                            };

                        $.each(result['data'], function( index, value ) {
//                          alert( value['type'] + ": " + value['message'] );
                            var funName='readpolicy("'+value['id']+'");';
                            if(value['type']==="Unsubscribe" || value['type']==="Delete Model" || value['type']==="Delete Twitt" ){
                              //  toastr.error(value['message'], 'Inconceivable!', opts);
                                toastr.error(value['message'], "<button type='button' onclick='"+funName+"' class='btn clear'>Close</button>", opts);
                                toastr.options.closeButton = true;
                                toastr.options.onCloseClick = function() { console.log('close button clicked');}
                            }
                            else if(value['type']==="Subscription" || value['type']==="Create Twitt" || value['type']==="Liked"){

                                toastr.success(value['message'], "<button type='button' onclick='"+funName+"' class='btn btn-danger clear pull-right btn-msg-close'>Close</button>", opts);
                                toastr.options.closeButton = true;
                                toastr.options.onCloseClick = function() { console.log('close button clicked');}
                            }
                            

                        });
                     }
                });
    if (typeof(myLib) == 'undefined') {
      setTimeout(initMyLib, 120000);
    } else {
      useMyLib();
    }
  }
      
  initMyLib(); //-> initMyLib is undefined
})(this, document);

function readpolicy(id){
var setUrl="<?php echo $this->Url->build(['controller'=>'Users','action'=>'readnotification']); ?>";
   $.ajax({
      url:setUrl,dataType:'json',data:{id:id},method:'post', success: function(result){
                    
             }//end success
        });
}//end readpolicy


    </script>
</body>
</html>
