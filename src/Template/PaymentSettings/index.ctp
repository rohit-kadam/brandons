<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentSetting[]|\Cake\Collection\CollectionInterface $paymentSettings
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Payment Setting'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="paymentSettings index large-9 medium-8 columns content">
    <h3><?= __('Payment Settings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_card_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_card_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expiration_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('security_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($paymentSettings as $paymentSetting): ?>
            <tr>
                <td><?= $this->Number->format($paymentSetting->id) ?></td>
                <td><?= $paymentSetting->has('user') ? $this->Html->link($paymentSetting->user->id, ['controller' => 'Users', 'action' => 'view', $paymentSetting->user->id]) : '' ?></td>
                <td><?= h($paymentSetting->credit_card_type) ?></td>
                <td><?= h($paymentSetting->credit_card_number) ?></td>
                <td><?= h($paymentSetting->expiration_date) ?></td>
                <td><?= h($paymentSetting->security_date) ?></td>
                <td><?= h($paymentSetting->created) ?></td>
                <td><?= h($paymentSetting->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $paymentSetting->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $paymentSetting->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $paymentSetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentSetting->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
