<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentSetting $paymentSetting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Payment Setting'), ['action' => 'edit', $paymentSetting->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Payment Setting'), ['action' => 'delete', $paymentSetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentSetting->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Payment Settings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Payment Setting'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="paymentSettings view large-9 medium-8 columns content">
    <h3><?= h($paymentSetting->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $paymentSetting->has('user') ? $this->Html->link($paymentSetting->user->id, ['controller' => 'Users', 'action' => 'view', $paymentSetting->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Credit Card Type') ?></th>
            <td><?= h($paymentSetting->credit_card_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Credit Card Number') ?></th>
            <td><?= h($paymentSetting->credit_card_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($paymentSetting->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expiration Date') ?></th>
            <td><?= h($paymentSetting->expiration_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Security Date') ?></th>
            <td><?= h($paymentSetting->security_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($paymentSetting->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($paymentSetting->modified) ?></td>
        </tr>
    </table>
</div>
