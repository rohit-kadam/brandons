<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Payment Settings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="paymentSettings form large-9 medium-8 columns content">
    <?= $this->Form->create($paymentSetting) ?>
    <fieldset>
        <legend><?= __('Add Payment Setting') ?></legend>
        <?php
//            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('credit_card_type');
            echo $this->Form->control('credit_card_number');
            echo $this->Form->control('expiration_date');
            echo $this->Form->control('security_code');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
