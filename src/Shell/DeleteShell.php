<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class DeleteShell extends Shell
{	
	   public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Subscriptions');
    }

    public function main()
    {

       $deleted_users= $this->Users->find('all')->where(['deleted_date >='=>date("Y-m-d",strtotime("+ days")),'role_id'=>2]);
	   if($deleted_users->count()>0){
	   		foreach ($deleted_users as $user) {
	   			$followers= $this->Subscriptions->find('all',['fields'=>['Users.email'],'contain'=>['Users']])->where(['follower_id'=>$user->id]);

	   			if($followers->count()>0){
	   			  foreach ($followers as $normal_user) {
		   				$message ='<html> <head> <title>'.Configure::read('projectTitle').' | '.ucwords($user->fullname).' will has deleted till date'.date("Y-m-d",strtotime($user->deleted_date)).' </title> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <style type="text/css"> body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; } table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; } img { -ms-interpolation-mode: bicubic; } img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; } table { border-collapse: collapse !important; } body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; } a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; } @media screen and (max-width: 480px) { .mobile-hide { display: none !important; } .mobile-center { text-align: center !important; } .mobile-table { width: 100% !important; } .text-center { text-align: center !important; } } div[style*="margin: 16px 0;"] { margin: 0 !important; } </style> </head> <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee"> <table style="border:2px solid #ef4e23;margin:20px;" align="center" border="0" cellpadding="0" cellspacing="0" width="768" class="mobile-table"> <tr> <td align="center" valign="top" style="font-size:0; padding: 35px 35px 0px 35px;" bgcolor="#ffffff"> <img width="120" src="http://dev.bitwaretechnologies.com/cakephp/Brandons/img/logo.png" /> </td> </tr>';
		                $message .=' <tr> <td align="center" style="padding: 0px 15px 20px 15px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 0px;"> <br> <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin:0px;" mc:edit="Headline"> Hello,<br>Welcome to '.Configure::read('projectTitle').' </h2> </td> </tr>';
		                $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 20px; padding-top:0px;" mc:edit="Paragraph"> <p style="text-align:center;line-height: 0px; color: #777777;"> '.ucwords($user->fullname).' Model will has deleted till '.date("Y-m-d",strtotime($user->deleted_date)).' </p> </td> </tr>';
		                $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 16px; padding-top: 0px;" mc:edit="Paragraph"> <p style="text-align:left;font-size: 16px;font-weight: 400;line-height: 16px;color: #777777;margin-top:50px;text-align:center;"> Please dont make payment to this model. </p> </td> </tr>';
		                $message .=' </table></td> </tr> <tr> <td align="center" style="padding:0px 35px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 0px 0 10px 0;" mc:edit="Address"> <p style="font-size: 14px; font-weight: 800; line-height:0px; color: #333333;"> <strong>Copyright &copy; '.date('Y').' <a href="http://dev.bitwaretechnologies.com/cakephp/Brandons/users/login">'.Configure::read('projectTitle').'</a>.</strong> All rights reserved. </p> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </body> </html>';

		                $email = new Email('default');

		                   if($email->from([Configure::read('from_email') => Configure::read('projectTitle').' | Delete acount'])
		                    ->emailFormat('html')
		                    ->to($normal_user->Users->email)
		                    ->subject(Configure::read('projectTitle').' | Model User delete account')
		                    ->send($message))
		                     {
		                         $this->out(' Email has been sent on your email.');    
		                           
		                    } 
		                     else{
		                           $this->out('Email is not sent.');                 
		                     }  
		   			}//end normal user iteration loop
	   			}//end followers count
	   			
	   		}//end foreach
	   }//end count main if
	         	
	   // delete all deleted_date users
	    $usersTable = TableRegistry::get('users');
	    $query = $usersTable->query();
		$query->update()
		    ->set(['status' => 'Inactive'])
		    ->where(['deleted_date' => date("Y-m-d"),'status'=>'Active'])
		    ->execute();
		$this->out(date("Y-m-d"));    
    }//end mmain()
}