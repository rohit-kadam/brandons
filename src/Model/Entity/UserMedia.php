<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserMedia Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $id_proof
 * @property string $pics
 * @property string $body_pics
 * @property string $face_pics
 * @property string $videos
 * @property string $model_attribute
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserMedia extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'id_proof' => true,
        'pics' => true,
        'body_pics' => true,
        'face_pics' => true,
        'videos' => true,
        'model_attribute' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
