<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentSetting Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $credit_card_type
 * @property string $credit_card_number
 * @property \Cake\I18n\FrozenDate $expiration_date
 * @property \Cake\I18n\FrozenDate $security_date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class PaymentSetting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'credit_card_type' => true,
        'credit_card_number' => true,
        'expiration_date' => true,
        'security_code' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
