<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $confirmpassword
 * @property string $status
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'firstname' => true,
        'lastname' => true,
        'username' => true,
        'role_id'=>true,
        'email' => true,
        'password' => true,
        'confirmpassword' => false,
        'aboutus'=> true,
        'amount'=> true,
        'phone'=> true,
        'datebirth' => true,
        'website' => true,
        'address' => true,
        'city' => true,
        'state' => true,
        'country' => true,
        'referral_id' => true,
        'referrer_id' => true,
        'is_live' => true,
        'verification_code' => true,
        'is_feature_models'=>true,
        'mykeywords' => true,
        'intrestedkeywords'=> true,
        'status' => true,
        'deleted_date'=>true,
        'modified' => true,
        'created' => true
    ];


    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected $_virtual=['full_name'];

   protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
          return (new DefaultPasswordHasher)->hash($password);
        }
    }

     protected function _getFullName(){
        return $this->_properties['firstname'].' '.$this->_properties['lastname'];
      /* return  (!empty($this->_properties['name'])?$this->_properties['name']:'').' '.(!empty($this->_properties['lastname'])?$this->_properties['lastname']:'');*/
    } 

}
