<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Referrals
 * @property |\Cake\ORM\Association\BelongsTo $Referrers
 * @property |\Cake\ORM\Association\HasMany $Comments
 * @property |\Cake\ORM\Association\HasMany $EmailNotifications
 * @property |\Cake\ORM\Association\HasMany $FeatureModels
 * @property |\Cake\ORM\Association\HasMany $Followers
 * @property |\Cake\ORM\Association\HasMany $Likes
 * @property |\Cake\ORM\Association\HasMany $Payments
 * @property |\Cake\ORM\Association\HasMany $Posts
 * @property |\Cake\ORM\Association\HasMany $Subscriptions
 * @property |\Cake\ORM\Association\HasMany $TweetNotifications
 * @property |\Cake\ORM\Association\HasMany $UserMedias
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->belongsTo('Referrals', [
        //     'foreignKey' => 'referral_id',
        //     'joinType' => 'INNER'
        // ]);
        // $this->belongsTo('Referrers', [
        //     'foreignKey' => 'referrer_id',
        //     'joinType' => 'INNER'
        // ]);
         $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('EmailNotifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('FeatureModels', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Followers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Subscriptions', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TweetNotifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserMedias', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

       $validator
            ->requirePresence('firstname', 'create')
            ->notEmpty('firstname')
            ->add('firstname','specialChars',[
                  'rule'=>function($value, $context){
                      if(preg_match("/^[A-Za-z\\- \']+$/",$value)){
                        return true;
                      } 
                  },
                  'message'=>' Special character not allow. '      
                ])
            ->add('firstname', 'length', [
                'rule' => ['minLength', 2],
                'message' => 'First name should be minimun 2 charachters long.'
            ]);

        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname')
            ->add('lastname','specialChars',[
                  'rule'=>function($value, $context){
                      if(preg_match("/^[A-Za-z\\- \']+$/",$value)){
                        return true;
                      } 
                  },
                  'message'=>' Special character not allow. '      
                ])
            ->add('lastname', 'length', [
                'rule' => ['minLength', 2],
                'message' => 'Last name should be minimun 2 charachters long.'
            ]); 

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username','specialChars',[
                  'rule'=>function($value, $context){
                      if(preg_match("/^[A-Za-z\\- \']+$/",$value)){
                        return true;
                      } 
                  },
                  'message'=>' Special character not allow. '      
                ])
            ->add('username', 'length', [
                'rule' => ['minLength', 8],
                'message' => 'Username should be minimun 8 charachters long.'
            ]);     

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail must be valid'
            ]);

        $validator
            ->scalar('password')
            ->lengthBetween('password', [8, 20],' Password must be between 8 to 20 character long. ')
            ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', 'passwordsWordSpaces', [
                'rule' => function ($value, $context) {
                        return ($value == trim($value) && strpos($value, ' ') === false);
                },
                'message' => ' Password does not allow spaces. '
            ]);

        $validator
            ->scalar('confirmpassword')
            ->lengthBetween('confirmpassword', [8, 20],' Confirm password must be between 8 to 20 character long. ')
            ->requirePresence('confirmpassword', 'create')
            ->notEmpty('confirmpassword')
            ->add('confirmpassword', 'passwordsEqual', [
                'rule' => function ($value, $context) {
                    return
                        isset($context['data']['password']) &&
                        $context['data']['password'] === $value;
                },
                'message' => ' Password does not match the confirm password. '
            ])
            ->add('confirmpassword', 'passwordsWordSpaces', [
                'rule' => function ($value, $context) {
                        return ($value == trim($value) && strpos($value, ' ') === false);
                },
                'message' => ' Confirm Password does not allow spaces. '
            ]);

       /* $validator
            ->numeric('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');*/

        $validator
            ->scalar('phone')
            ->requirePresence('phone', 'create')
            ->notEmpty('phone')
            ->lengthBetween('phone', [10, 20],' Phone number must be between 10 to 20 digits long. ');
        return $validator;
    }

    public function validationRegister(Validator $validator)
    {   

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

       $validator
            ->requirePresence('firstname', 'create')
            ->notEmpty('firstname')
            ->add('firstname','specialChars',[
                  'rule'=>function($value, $context){
                      if(preg_match("/^[A-Za-z\\- \']+$/",$value)){
                        return true;
                      } 
                  },
                  'message'=>' Special character not allow. '      
                ])
            ->add('firstname', 'length', [
                'rule' => ['minLength', 2],
                'message' => 'First name should be minimun 2 charachters long.'
            ]);

        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname')
            ->add('lastname','specialChars',[
                  'rule'=>function($value, $context){
                      if(preg_match("/^[A-Za-z\\- \']+$/",$value)){
                        return true;
                      } 
                  },
                  'message'=>' Special character not allow. '      
                ])
            ->add('lastname', 'length', [
                'rule' => ['minLength', 2],
                'message' => 'Last name should be minimun 2 charachters long.'
            ]);
              
         $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username','specialChars',[
                  'rule'=>function($value, $context){
                      if(preg_match("/^[A-Za-z\\- \']+$/",$value)){
                        return true;
                      } 
                  },
                  'message'=>' Special character not allow. '      
                ])
            ->add('username', 'length', [
                'rule' => ['minLength', 8],
                'message' => 'Username should be minimun 8 charachters long.'
            ]); 

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail must be valid'
            ]);

        $validator
            ->scalar('password')
            ->lengthBetween('password', [8, 20],' Password must be between 8 to 20 character long. ')
            ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', 'passwordsWordSpaces', [
                'rule' => function ($value, $context) {
                        return ($value == trim($value) && strpos($value, ' ') === false);
                },
                'message' => ' Password does not allow spaces. '
            ]);

        $validator
            ->scalar('confirmpassword')
            ->lengthBetween('confirmpassword', [8, 20],' Confirm password must be between 8 to 20 character long. ')
            ->requirePresence('confirmpassword', 'create')
            ->notEmpty('confirmpassword')
            ->add('confirmpassword', 'passwordsEqual', [
                'rule' => function ($value, $context) {
                    return
                        isset($context['data']['password']) &&
                        $context['data']['password'] === $value;
                },
                'message' => ' Password does not match the confirm password. '
            ])
            ->add('confirmpassword', 'passwordsWordSpaces', [
                'rule' => function ($value, $context) {
                        return ($value == trim($value) && strpos($value, ' ') === false);
                },
                'message' => ' Confirm Password does not allow spaces. '
            ]);
            
        $validator
            ->requirePresence('pics', 'create')
            ->notEmpty('pics')
            ->add('pics', [
                        'validExtension' => [
                            'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
                        ]
            ]);

            $validator
            ->requirePresence('body_pics', 'create')
            ->notEmpty('body_pics')
            ->add('body_pics', [
                        'validExtension' => [
                            'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
                        ]
            ]);

            $validator
            ->requirePresence('face_pics', 'create')
            ->notEmpty('face_pics')
            ->add('face_pics', [
                        'validExtension' => [
                            'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
                        ]
            ]);

            $validator
            ->requirePresence('videos', 'create')
            ->notEmpty('videos')
            ->add('videos', [
                        'validExtension' => [
                            'rule' => ['extension',['mpeg','mp4','mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .mpeg, .mp4, .mp3 ')
                        ]
            ]);

            $validator
            ->requirePresence('id_proof', 'create')
            ->notEmpty('id_proof')
            ->add('id_proof', [
                        'validExtension' => [
                            'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
                        ]
            ]);

            
            return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        //$rules->add($rules->existsIn(['referrer_id'], 'Referrers'));

        return $rules;
    }
}
