<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CommentsTable|\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\LikesTable|\Cake\ORM\Association\HasMany $Likes
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'post_id'
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'post_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('post_type')
            ->requirePresence('post_type', 'create')
            ->notEmpty('post_type');

        // $validator
        //     ->scalar('content')
        //     ->requirePresence('content', 'create')
        //     ->notEmpty('content');

        // $validator
        //     ->scalar('media')
        //     ->requirePresence('media', 'create')
        //     ->notEmpty('media');

        return $validator;
    }


    public function validationPosttype(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('post_type')
            ->requirePresence('post_type', 'create')
            ->notEmpty('post_type');

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        // $validator
        //     ->scalar('media')
        //     ->requirePresence('media', 'create')
        //     ->notEmpty('media');

        return $validator;
    }

    public function validationPhotostype(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('post_type')
            ->requirePresence('post_type', 'create')
            ->notEmpty('post_type');

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->scalar('media')
            ->requirePresence('media', 'create')
            ->notEmpty('media')
            ->add('media', [
                        'validExtension' => [
                            'rule' => ['extension',['png','jpeg','jpg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .png, .jpeg, .jpg ')
                        ]
            ]);

        return $validator;
    }

    public function validationVideostype(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('post_type')
            ->requirePresence('post_type', 'create')
            ->notEmpty('post_type');

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->scalar('media')
            ->requirePresence('media', 'create')
            ->notEmpty('media')
            ->add('media', [
                        'validExtension' => [
                            'rule' => ['extension',['mpeg','mp4','mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
                            'message' => __('These files extension are allowed: .mpeg, .mp4, .mp3 ')
                        ]
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
