<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Referrals
 * @property |\Cake\ORM\Association\BelongsTo $Referrers
 * @property |\Cake\ORM\Association\HasMany $Comments
 * @property |\Cake\ORM\Association\HasMany $EmailNotifications
 * @property |\Cake\ORM\Association\HasMany $FeatureModels
 * @property |\Cake\ORM\Association\HasMany $Followers
 * @property |\Cake\ORM\Association\HasMany $Likes
 * @property |\Cake\ORM\Association\HasMany $Payments
 * @property |\Cake\ORM\Association\HasMany $Posts
 * @property |\Cake\ORM\Association\HasMany $Subscriptions
 * @property |\Cake\ORM\Association\HasMany $TweetNotifications
 * @property |\Cake\ORM\Association\HasMany $UserMedias
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NotificationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('notifications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

   
}
