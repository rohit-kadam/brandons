<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserMedias Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserMedia get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserMedia newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserMedia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserMedia|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserMedia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserMedia[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserMedia findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserMediasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_medias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->requirePresence('pics', 'create')
        //     ->notEmpty('pics')
        //     ->add('pics', [
        //                 'validExtension' => [
        //                     'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
        //                     'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
        //                 ]
        //     ]);

        //     $validator
        //     ->requirePresence('body_pics', 'create')
        //     ->notEmpty('body_pics')
        //     ->add('body_pics', [
        //                 'validExtension' => [
        //                     'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
        //                     'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
        //                 ]
        //     ]);

        //     $validator
        //     ->requirePresence('face_pics', 'create')
        //     ->notEmpty('face_pics')
        //     ->add('face_pics', [
        //                 'validExtension' => [
        //                     'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
        //                     'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
        //                 ]
        //     ]);

        //     $validator
        //     ->requirePresence('videos', 'create')
        //     ->notEmpty('videos')
        //     ->add('videos', [
        //                 'validExtension' => [
        //                     'rule' => ['extension',['mpeg','mp4','mp3']], // default  ['gif', 'jpeg', 'png', 'jpg']
        //                     'message' => __('These files extension are allowed: .mpeg, .mp4, .mp3 ')
        //                 ]
        //     ]);

        //     $validator
        //     ->requirePresence('id_proof', 'create')
        //     ->notEmpty('id_proof')
        //     ->add('id_proof', [
        //                 'validExtension' => [
        //                     'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
        //                     'message' => __('These files extension are allowed: .png, .jpg, .jpeg')
        //                 ]
        //     ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
