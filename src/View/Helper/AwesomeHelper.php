<?php 
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

class AwesomeHelper extends Helper
{

      public $helpers = ['Html','Url','Form','Paginator'];

      public function getPagination(){
              echo  '<div class="paginator">';
               if($this->Paginator->counter('{{count}}')>10){                   
                   echo  '<div class="col-sm-5">
                      <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing '. $this->Paginator->counter() .' entries</div>
                    </div>';
                   echo '<div class="col-sm-7">   
                        <div class="dataTables_paginate paging_simple_numbers pull-right">
                            <ul class="pagination">';
                            echo $this->Paginator->prev('' . __('Previous'));
                                echo $this->Paginator->numbers();
                                echo $this->Paginator->next(__('Next') . '') ;

                           echo '</ul>
                        </div>
                    </div>';       
                   }//end if 
          echo '</div>';

      }//end getPagination


      public function getLikeHml($liked=0, $unliked=0, $post_id, $user_id){

        echo $get_rate=$this->_View->cell('Review::display',[$post_id,$user_id]);
      }

      public function getPostHtml($posts){

        $PostHtml= '<div class="post-content">
                <div class="row">';
                      if($posts->count()>0) :
                         foreach ($posts as $post):
                          $PostHtml .= '<div class="box box-widget m-top40">
                              <div class="box-header with-border">
                                <div class="date-box"><i class="fa fa-clock-o" aria-hidden="true"></i> '.date("Y-m-d , H:i:s",strtotime($post->modified)).'</div>
                                <div class="user-img-p">
                                  <div>'.$this->Html->image('profile/'.$post->user->user_medias[0]->face_pics,['class'=>'pull-left img-responsive img-circle msg-img-style ip-img-mn']).ucwords($post->user->full_name).'</div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="user-block-pcust block-head">
                                  <span class="username2">'.ucfirst($post->content).'</span>
                                </div>
                              </div>';
                              $PostHtml .= '<p>';

                                if($post->post_type=="PHOTOS"){
                                $PostHtml .= '<img src="'.$this->request->webroot.'medias/'.$post->media.'" class="img-responsive" style="width:100%">';
                                }
                                else if($post->post_type=="VIDEOS"){ 

                                $PostHtml .= '<video width="100%" height="240" controls>
                                  <source src="'.$this->request->webroot.'medias/'.$post->media.'" type="video/mp4">
                                  Your browser does not support the video tag.
                                </video>';

                                }

                          $liked=0;
                          $unliked=0;
                          if(!empty($post->likes)):
                           foreach ($post->likes as $like) {
                            if($like->liked=="Liked"){
                              $liked++;
                            }

                            if($like->liked=="Unlike"){
                              $unliked++;
                            }

                          } 
                        endif;

                               $PostHtml .= '<div class="l-c-s">
                                  <ul class="list-unstyled">
                                    '.$this->_View->cell('Review::display',[$post->id, $this->request->session()->read('Auth.User.id')]);
                                    // <li><a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> Comment</a></li>
                                    // <li><a href="#"><i class="fa fa-share" aria-hidden="true"></i> Share</a></li>
                                  $PostHtml .='</ul>
                                </div>
                                <div class="post-details-outer">';
                                    
                                    //Comments
                                  if(!empty($post->comments)):
                                    
                                    foreach ($post->comments as $comment) {
                                      $PostHtml .='<div class="post-comment">';
                                      $PostHtml .= '<div class="img-left-div">'.$this->Html->image('profile/'.$comment->user->user_medias[0]->face_pics ,['class'=>'img-responsive img-circle msg-img-style profile-photo-comm']).'</div>';
                                        $PostHtml .='<div class="rht-div">'.nl2br($comment->comment).'</div>';
                                        // $PostHtml .='<div class="delete-post pull-right">'.'<span class="fa fa-times"></span>'.'</div>';
                                      $PostHtml .= '</div>';  
                                    } 
                                  endif;
                                 
                                 // end comments

                                 // create comments
                                  $PostHtml .= '<div class="post-comment">';
                                    $PostHtml .= '<div class="col-md-2 nopad-both">'.$this->Html->image('user2-160x160.jpg',['class'=>'img-responsive img-circle msg-img-style profile-photo-comm1']).'</div>';
                                      $PostHtml .= '<div class="col-md-8 nopad-both">'.$this->Form->create(null,['url'=>['controller'=>'comments','action'=>'add']],['enctype'=>'multipart/form-data','id'=>'createcommentsForms']) ;
                                       $PostHtml .= '<div>';
                                       $PostHtml .=  $this->Form->control('comment',['type'=>'textarea','class'=>'form-control input-form-cmncust','placeholder'=>'Write a comment','rows'=>'2','label'=>false]);
                                       $PostHtml .=  $this->Form->hidden('post_id',['value'=>$post->id]);
                                          //$PostHtml .= '<span class="input-group-addon input-gp-addcust"><i class="fa fa-picture-o c-icon1" aria-hidden="true"></i> <i class="c-icon2 fa fa-paper-plane" aria-hidden="true"></i></span>';
                                        $PostHtml .= '</div></div>';
                                        $PostHtml .= '<div class="btn-post-outer col-md-2 nopad-both">'.$this->Form->button($this->Html->tag("i", "", array("class" => "fa fa-paper-plane-o")),['class'=>'btn btn-danger btn-publish1']).'</div>';
                                      $PostHtml .= $this->Form->end();
                                  $PostHtml .='</div>
                                </div>
                              </p>
                          </div>';  
                           endforeach;
                          endif;   
                $PostHtml .='</div>
            </div>';

            echo $PostHtml;
      }//end getPostHtml


      public function getChats($getData){
            echo '<div class="quick-contact">
            <a class="btn btn-primary btn-xs btn-quick collapsed" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
              <i class="fa fa-commenting-o" aria-hidden="true"></i> Chats
            </a>

            <div class="collapse" id="collapseExample" style="height: 0px;">
                  <div class="inner-well">
                      <div class="follow-user">
                        <span class="online"></span>
                        '.$this->Html->image('profile/'.$getData->user_medias[0]['face_pics'],['class'=>'profile-photo-sm1 img-responsive pull-left']).'
                          <div class="user-onlinenm">
                            <h5 class="right-panel-head1"><a class="user-online" href="#">Diana Amber</a></h5>
                          </div>
                      </div>
                      <div class="follow-user">
                        <span class="online"></span>
                        '. $this->Html->image('profile/'.$getData->user_medias[0]['face_pics'],['class'=>'profile-photo-sm1 img-responsive pull-left']).'
                          <div class="user-onlinenm">
                            <h5 class="right-panel-head1"><a class="user-online" href="#">Alex Timber</a></h5>
                          </div>
                      </div>
                      <div class="follow-user">
                        <span class="online"></span>
                        '.$this->Html->image('profile/'.$getData->user_medias[0]['face_pics'],['class'=>'profile-photo-sm1 img-responsive pull-left']).'                          <div class="user-onlinenm">
                            <h5 class="right-panel-head1"><a class="user-online" href="#">David Tumbro</a></h5>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
            <div class="quick-contact1">
              <a class="btn btn-primary btn-xs btn-quick1" data-toggle="collapse" href="#collapseExample1" aria-expanded="true" aria-controls="collapseExample1">
                <div class="pull-left left-secp1"><i class="fa fa-commenting-o" aria-hidden="true"></i> Diana Timber</div>
                <div class="pull-right right-secp1"><button type="button" class="btn btn-box-tool pull-right" data-widget="remove"><i class="fa fa-times"></i></button></div>
                <div class="clearfix"></div>
              </a>
              <div class="collapse in" id="collapseExample1" style="" aria-expanded="true">
                <div class="inner-well">
                  <div class="box with-no-border direct-chat direct-chat-danger">
                    <div class="direct-chat-messages">
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                              <span class="direct-chat-name pull-left">Alexander Pierce</span>
                              <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                            </div>
                            '.$this->Html->image('user-1.jpg',['class'=>'img-responsive img-circle direct-chat-img']).'
                            <div class="direct-chat-text">Hi, How are you buddy? </div>
                        </div>
                        <div class="direct-chat-msg right">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                              </div>
                              '.$this->Html->image('user2-160x160.jpg',['class'=>'img-responsive img-circle direct-chat-img']).'
                              <div class="direct-chat-text">Hey, I am good. and you?</div>
                        </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <form action="#" method="post">
                      <div class="input-group">
                        <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-btn">
                              <button type="submit" class="btn btn-danger btn-flat">Send</button>
                            </span>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>';

    }//getChats


    public function getNotification($selectedtab=null,$notifications=null){
        
        if($selectedtab=="all"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="'.$this->Url->build(['controller'=>'notifications','action'=>'index']).'" class="btn '.$class.'" data-toggle="tooltip" data-placement="left" title="Tooltip on left">All</a>';

        if($selectedtab=="interactions"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="#" class="btn '.$class.'" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Interactions</a>';

        if($selectedtab=="liked"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="'.$this->Url->build(['controller'=>'notifications','action'=>'liked']).'" class="btn '.$class.'" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">Liked</a>';

        if($selectedtab=="subscribed"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="'.$this->Url->build(['controller'=>'notifications','action'=>'subscribed']).'" class="btn '.$class.'" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">Subscribed</a>';

        if($selectedtab=="tipped"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="#" class="btn '.$class.'" data-toggle="tooltip" data-placement="right" title="Tooltip on right">Tipped</a>';

        if($selectedtab=="price changes"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="#" class="btn '.$class.'" data-toggle="tooltip" data-placement="right" title="Tooltip on right">Price Changes</a>';

        if($selectedtab=="alert"){ $class="btn-danger"; }else{ $class="btn-default"; }
        echo '<a href="#" class="btn '.$class.'" data-toggle="tooltip" data-placement="right" title="Tooltip on right">Alert</a>';

         if($notifications->count()>0):
         foreach ($notifications as $notification):
                    echo '<p>'.$notification->message.'</p>';
          endforeach;
        endif;

    }//end getNotification

}
?>