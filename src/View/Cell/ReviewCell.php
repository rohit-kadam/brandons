<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\View\CellTrait;
/**
 * Inbox cell
 */
class ReviewCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($post_id=null, $user_id=null)
    {
       $this->loadModel('Likes');
       $is_like=$this->Likes->find('all')->where(['post_id'=>$post_id,'user_id'=>$user_id, 'liked'=>'Liked'])->count();
       $is_unlike=$this->Likes->find('all')->where(['post_id'=>$post_id,'user_id'=>$user_id, 'liked'=>'Unlike'])->count();
       $liked= $this->Likes->find('all')->where(['post_id'=>$post_id, 'liked'=>'Liked'])->count();
       $unliked= $this->Likes->find('all')->where(['post_id'=>$post_id, 'liked'=>'Unlike'])->count();
       $this->set(compact('liked','unliked','is_like','is_unlike','post_id'));    
    }
}//end 
