<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserMedias Controller
 *
 * @property \App\Model\Table\UserMediasTable $UserMedias
 *
 * @method \App\Model\Entity\UserMedia[] paginate($object = null, array $settings = [])
 */
class UserMediasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userMedias = $this->paginate($this->UserMedias);

        $this->set(compact('userMedias'));
        $this->set('_serialize', ['userMedias']);
    }

    /**
     * View method
     *
     * @param string|null $id User Media id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userMedia = $this->UserMedias->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userMedia', $userMedia);
        $this->set('_serialize', ['userMedia']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userMedia = $this->UserMedias->newEntity();
        if ($this->request->is('post')) {
            $userMedia = $this->UserMedias->patchEntity($userMedia, $this->request->getData());
            if ($this->UserMedias->save($userMedia)) {
                $this->Flash->success(__('The user media has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user media could not be saved. Please, try again.'));
        }
        $users = $this->UserMedias->Users->find('list', ['limit' => 200]);
        $this->set(compact('userMedia', 'users'));
        $this->set('_serialize', ['userMedia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Media id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userMedia = $this->UserMedias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userMedia = $this->UserMedias->patchEntity($userMedia, $this->request->getData());
            if ($this->UserMedias->save($userMedia)) {
                $this->Flash->success(__('The user media has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user media could not be saved. Please, try again.'));
        }
        $users = $this->UserMedias->Users->find('list', ['limit' => 200]);
        $this->set(compact('userMedia', 'users'));
        $this->set('_serialize', ['userMedia']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Media id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userMedia = $this->UserMedias->get($id);
        if ($this->UserMedias->delete($userMedia)) {
            $this->Flash->success(__('The user media has been deleted.'));
        } else {
            $this->Flash->error(__('The user media could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
