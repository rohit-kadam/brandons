<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TweetNotifications Controller
 *
 * @property \App\Model\Table\TweetNotificationsTable $TweetNotifications
 *
 * @method \App\Model\Entity\TweetNotification[] paginate($object = null, array $settings = [])
 */
class TweetNotificationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $tweetNotifications = $this->paginate($this->TweetNotifications);

        $this->set(compact('tweetNotifications'));
        $this->set('_serialize', ['tweetNotifications']);
    }

    /**
     * View method
     *
     * @param string|null $id Tweet Notification id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tweetNotification = $this->TweetNotifications->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('tweetNotification', $tweetNotification);
        $this->set('_serialize', ['tweetNotification']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tweetNotification = $this->TweetNotifications->newEntity();
        if ($this->request->is('post')) {
            $tweetNotification = $this->TweetNotifications->patchEntity($tweetNotification, $this->request->getData());
            if ($this->TweetNotifications->save($tweetNotification)) {
                $this->Flash->success(__('The tweet notification has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tweet notification could not be saved. Please, try again.'));
        }
        $users = $this->TweetNotifications->Users->find('list', ['limit' => 200]);
        $this->set(compact('tweetNotification', 'users'));
        $this->set('_serialize', ['tweetNotification']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tweet Notification id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tweetNotification = $this->TweetNotifications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tweetNotification = $this->TweetNotifications->patchEntity($tweetNotification, $this->request->getData());
            if ($this->TweetNotifications->save($tweetNotification)) {
                $this->Flash->success(__('The tweet notification has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tweet notification could not be saved. Please, try again.'));
        }
        $users = $this->TweetNotifications->Users->find('list', ['limit' => 200]);
        $this->set(compact('tweetNotification', 'users'));
        $this->set('_serialize', ['tweetNotification']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tweet Notification id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tweetNotification = $this->TweetNotifications->get($id);
        if ($this->TweetNotifications->delete($tweetNotification)) {
            $this->Flash->success(__('The tweet notification has been deleted.'));
        } else {
            $this->Flash->error(__('The tweet notification could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
