<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Core\Configure;

class CommonComponent extends Component{
    
    public $components = ['Flash'];

    public function get_email_template_registered($emailid=null, $password=null,$registerFor=null, $verification_code=null){

          $message ='<html> <head> <title>'.Configure::read('projectTitle').' | Account Verification</title> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <style type="text/css"> body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; } table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; } img { -ms-interpolation-mode: bicubic; } img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; } table { border-collapse: collapse !important; } body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; } a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; } @media screen and (max-width: 480px) { .mobile-hide { display: none !important; } .mobile-center { text-align: center !important; } .mobile-table { width: 100% !important; } .text-center { text-align: center !important; } } div[style*="margin: 16px 0;"] { margin: 0 !important; } </style> </head> <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="768" class="mobile-table"> <tr> <td align="center" valign="top" style="font-size:0; padding: 35px 35px 0px 35px;" bgcolor="#ffffff"> <img width="120" src="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'img/formtop.png" /> </td> </tr>';
          $message .=' <tr> <td align="center" style="padding: 0px 15px 20px 15px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 0px;"> <br> <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin:0px;" mc:edit="Headline"> Hello,<br>Welcome to '.Configure::read('projectTitle').' </h2> </td> </tr>';
          $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;" mc:edit="Paragraph"> <p style="text-align:center;margin-bottom:30px;font-size: 20px; font-weight: 600; line-height: 18px; color: #777777;"> Your account details are: </p> </td> </tr>';
          $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;" mc:edit="Paragraph"> <p style="padding-left:146px;text-align:left;font-size: 16px; font-weight: 400; line-height: 18px; color: #777777;"> <strong>Username/Email:</strong> '.$emailid.' </p> </td> </tr>';
          $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;" mc:edit="Paragraph"> <p style="text-align:left;padding-left:146px;font-size: 16px; font-weight: 400; line-height: 30px; color: #777777;"> <strong>Password:</strong> '.$password.' </p> </td> </tr>';
          $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;" mc:edit="Paragraph"> <p style="padding-left: 316px;text-align:left;font-size: 16px;font-weight: 400;line-height: 18px;color: #777777;margin-top:60px;"> <a style="text-decoration:none;padding:10px;background:#5cb85c; color:#fff; border-color:#4cae4c;border-radius:4px; white-space:nowrap;" href="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'Users/verification/'.$verification_code.'" target="_blank">Active account</a></p> </td> </tr>';
          $message .=' </table></td> </tr> <tr> <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;" mc:edit="Address"> <p style="font-size: 14px; font-weight: 800; line-height: 18px; color: #333333;"> <strong>Copyright &copy; '.date('Y').' <a href="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'">'.Configure::read('projectTitle').'</a>.</strong> All rights reserved. </p> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </body> </html>';
        
        $email = new Email('default');
       if($email->from([Configure::read('from_email') => Configure::read('projectTitle').' | '.$registerFor.' Registered '])
        ->emailFormat('html')
        ->to($emailid)
        ->subject(Configure::read('projectTitle').' | '.$registerFor.' Registered ')
        ->send($message))
         {
             $this->Flash->success( $registerFor.' registered successfully.');                 
             return true;
         } 
         else{
               $this->Flash->error( $registerFor.' registered successfully, but email has not sent to registered user.');                 
              return false;                 
         }   
     //end send email        
    }//end getemailTemplate for registered user


    public function createNotification($user_id=null, $model_id=null, $notification_type=null, $message=null, $post_id=0 ){

        $notification = TableRegistry::get('Notifications');
        $createNotification= $notification->newEntity([
          'user_id' =>$user_id,
          'model_id' => $model_id,
          'post_id'=> $post_id,
          'notification_type'=> $notification_type,
          'message'=> $message,
          'status'=> 'Unread',
          'created' => date("Y-m-d H:i:s"),
          'modified' => date("Y-m-d H:i:s")
      ]);
        if($createNotification=$notification->save($createNotification)){
          $settings=TableRegistry::get('Users');
          $get_user_details = $settings->find('all')->where(['id'=>$model_id])->first();
          $this->sendgloblemessage($get_user_details['email'],$message);
          return true;

        }
        else{
           return false;
        }

    }//end Notification

    public function sendgloblemessage($email_id = null,$notification_msg = null){

        $message ='<html> <head> <title>'.Configure::read('projectTitle').' | Notification </title> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <style type="text/css"> body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; } table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; } img { -ms-interpolation-mode: bicubic; } img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; } table { border-collapse: collapse !important; } body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; } a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; } @media screen and (max-width: 480px) { .mobile-hide { display: none !important; } .mobile-center { text-align: center !important; } .mobile-table { width: 100% !important; } .text-center { text-align: center !important; } } div[style*="margin: 16px 0;"] { margin: 0 !important; } </style> </head> <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="768" class="mobile-table"> <tr> <td align="center" valign="top" style="font-size:0; padding: 35px 35px 0px 35px;" bgcolor="#ffffff"> <img width="120" src="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'img/formtop.png" /> </td> </tr>';
          $message .=' <tr> <td align="center" style="padding: 0px 15px 20px 15px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 0px;"> <br> <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin:0px;" mc:edit="Headline"> Hello,<br>Welcome to '.Configure::read('projectTitle').' </h2> </td> </tr>';
          $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;" mc:edit="Paragraph"> <p style="text-align:center;margin-bottom:30px;font-size: 20px; font-weight: 600; line-height: 18px; color: #777777;"> Notification Message: </p> </td> </tr>';

          $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;" mc:edit="Paragraph"> <p style="padding-left:146px;text-align:left;font-size: 16px; font-weight: 400; line-height: 18px; color: #777777;">'.nl2br($notification_msg).' </p> </td> </tr>';

       
          $message .=' </table></td> </tr> <tr> <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;" mc:edit="Address"> <p style="font-size: 14px; font-weight: 800; line-height: 18px; color: #333333;"> <strong>Copyright &copy; '.date('Y').' <a href="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'">'.Configure::read('projectTitle').'</a>.</strong> All rights reserved. </p> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </body> </html>';

         $email = new Email('default');
       if($email->from([Configure::read('from_email') => Configure::read('projectTitle').' | Notification'])
        ->emailFormat('html')
        ->to($email_id)
        ->subject(Configure::read('projectTitle').' | Notification ')
        ->send($message))
         {
            // $this->Flash->success( $registerFor.' registered successfully.');                 
             return true;
         } 
         else{
             //  $this->Flash->error( $registerFor.' registered successfully, but email has not sent to registered user.');                 
              return false;                 
         }   
    }
}//end 




?>