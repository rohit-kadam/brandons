<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cms Controller
 *
 * @property \App\Model\Table\CmsTable $Cms
 *
 * @method \App\Model\Entity\Cm[] paginate($object = null, array $settings = [])
 */
class CmsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $cms = $this->paginate($this->Cms);

        $this->set(compact('cms'));
        $this->set('_serialize', ['cms']);
    }

    /**
     * View method
     *
     * @param string|null $id Cm id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cm = $this->Cms->get($id, [
            'contain' => []
        ]);

        $this->set('cm', $cm);
        $this->set('_serialize', ['cm']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cm = $this->Cms->newEntity();
        if ($this->request->is('post')) {
            $cm = $this->Cms->patchEntity($cm, $this->request->getData());
            if ($this->Cms->save($cm)) {
                $this->Flash->success(__('The cm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cm could not be saved. Please, try again.'));
        }
        $this->set(compact('cm'));
        $this->set('_serialize', ['cm']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cm id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cm = $this->Cms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cm = $this->Cms->patchEntity($cm, $this->request->getData());
            if ($this->Cms->save($cm)) {
                $this->Flash->success(__('The cm has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cm could not be saved. Please, try again.'));
        }
        $this->set(compact('cm'));
        $this->set('_serialize', ['cm']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cm id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cm = $this->Cms->get($id);
        if ($this->Cms->delete($cm)) {
            $this->Flash->success(__('The cm has been deleted.'));
        } else {
            $this->Flash->error(__('The cm could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
