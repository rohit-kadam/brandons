<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Subscriptions Controller
 *
 * @property \App\Model\Table\SubscriptionsTable $Subscriptions
 *
 * @method \App\Model\Entity\Subscription[] paginate($object = null, array $settings = [])
 */
class SubscriptionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Followers']
        ];
        $subscriptions = $this->paginate($this->Subscriptions);

        $this->set(compact('subscriptions'));
        $this->set('_serialize', ['subscriptions']);
    }

    /**
     * View method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subscription = $this->Subscriptions->get($id, [
            'contain' => ['Users', 'Followers', 'Payments']
        ]);

        $this->set('subscription', $subscription);
        $this->set('_serialize', ['subscription']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($follower_id=null)
    {

        $check_isexist= $this->Subscriptions->find('all')->where(['Subscriptions.user_id'=>$this->Auth->user('id'),'follower_id' => $follower_id])->count();

        if($check_isexist>0){
            $this->Flash->warning(__(' This Model already subscribed. '));
          return $this->redirect($this->referer());
        }

        $subscription = $this->Subscriptions->newEntity();

        $followerData= [
            'user_id' => $this->Auth->user('id'),
            'follower_id' => $follower_id,
            'status'=>'Active' 
        ];

            $subscription = $this->Subscriptions->patchEntity($subscription, $followerData);
            if ($this->Subscriptions->save($subscription)) {
                # create notifications
                $this->Common->createNotification($this->Auth->user('id'), $follower_id, 'Subscription', $this->Auth->user('fullname').' has been subscribed to you. ');

                $this->Flash->success(__('The subscription has been saved.'));
            }
            else{
                $this->Flash->error(__('The subscription could not be saved. Please, try again.'));
            }
         return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subscription = $this->Subscriptions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subscription = $this->Subscriptions->patchEntity($subscription, $this->request->getData());
            if ($this->Subscriptions->save($subscription)) {
                $this->Flash->success(__('The subscription has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The subscription could not be saved. Please, try again.'));
        }
        $users = $this->Subscriptions->Users->find('list', ['limit' => 200]);
        $followers = $this->Subscriptions->Followers->find('list', ['limit' => 200]);
        $this->set(compact('subscription', 'users', 'followers'));
        $this->set('_serialize', ['subscription']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        $subscription = $this->Subscriptions->find('all')->where(['user_id'=>$this->Auth->user('id'),'follower_id'=>$id])->first();
        if ($this->Subscriptions->delete($subscription)) {
            # create notifications
            $this->Common->createNotification($this->Auth->user('id'), $id, 'Unsubscribe', $this->Auth->user('fullname').' has been cancel your subscription. ');

            $this->Flash->success(__('The subscription has been deleted.'));
        } else {
            $this->Flash->error(__('The subscription could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'followings']);
    }

    public function fans(){
        $fans= $this->Subscriptions->find('all',['contain'=>['Users'=>['UserMedias']]])->where(['follower_id'=>$this->Auth->user('id')]);
        $alreadyFollowed= $this->Subscriptions->find('all',['contain'=>['Users']])->where(['user_id'=>$this->Auth->user('id')]);
        $follower_idArr= array_map(function($e1){ return $e1['follower_id']; }, $alreadyFollowed->toArray());
        $this->set(compact('fans','follower_idArr'));
    }

    public function followings(){
        $this->loadModel('Users');

        $followings= $this->Subscriptions->find('all',['contain'=>['Users']])->where(['user_id'=>$this->Auth->user('id')]);
        $follower_Arr= array_map(function($e1){ return $e1['follower_id']; },$followings->toArray());

        $followings=array();
        if(!empty($follower_Arr)){
            $followings= $this->Users->find('all',['contain'=>['UserMedias']])->where(['id IN'=>$follower_Arr]);    
        }
        
        $this->set(compact('followings'));
    }

    // return total follower
    public function gettotalfollowers(){
        $countfollowers = $this->Subscriptions->find('all')->where(['follower_id'=>$this->request->id])->count();
         $this->response->body($countfollowers);
        return $this->response;
    }//end gettotalfollowers
}
