<?php
namespace App\Controller;

use App\Controller\AppController;

ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_execution_time', 0);
ini_set('set_time_limit', 0);
//require ROOT . DS . 'vendor' . DS . 'crop' . DS . 'crop.php';

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[] paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['getrequest']);
    }


  public function getrequest()
  {
    $this->autoRender=False;

    exit;
    /*print_r($_POST);
    print_r($_FILES);

    if(class_exists('CropAvatar'))
     {
      echo 'CropAvatar exist';
     } */
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Users', 'Comments', 'Likes']
        ]);

        $this->set('post', $post);
        $this->set('_serialize', ['post']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {   
      $this->loadModel('Users');
      $model= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.id'=>$this->Auth->user('id')])->first(); 

        $post = $this->Posts->newEntity();
         if ($this->request->is('ajax')) {
            $this->autoRender = false;
            
            $this->response->disableCache();

            $this->request->data['user_id']=$this->Auth->user('id');
            # if both file selected
            if($_FILES['photos']['size']>0 AND $_FILES['videos']['size']>0){
               echo json_encode(['result'=>'fail','data'=>['content'=>['_empty'=>" Please select either photos or videos. "]]]); exit;
            }

            # if content empty
            if(empty(trim($this->request->data['content']))){
               echo json_encode(['result'=>'fail','data'=>['content'=>['_empty'=>" Please enter content. "]]]); exit;
            }


            //start upload photos
           if(!empty($_FILES['photos'])){
             
                $photos=$this->request->data['photos'];
                $photosName = time().'_photos'.basename($photos["name"]);
                $targetDir = WWW_ROOT."/medias/";
                $targetFilePath = $targetDir . $photosName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('jpg','png','jpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($photos["tmp_name"], $targetFilePath)){
                        $this->request->data['media'] = $photosName;
                    }//end media
                }//end  
                //end upload media
          }//end media image

          //start upload videos
           if(!empty($_FILES['videos'])){
             
                $videos=$this->request->data['videos'];
                $videosName = time().'_videos'.basename($videos["name"]);
                $targetDir = WWW_ROOT."medias/";
                $targetFilePath = $targetDir . $videosName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('mp4','mp3','mpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($videos["tmp_name"], $targetFilePath)){
                        $this->request->data['media'] = $videosName;
                    }//end videos
                }//end  
                //end upload videos
          }//end videos image

            if($_FILES['photos']['size']>0){
               $validateby="Photostype"; 
               $this->request->data['post_type']='PHOTOS';
            }
            else if($_FILES['videos']['size']>0){
               $validateby="Videostype"; 
               $this->request->data['post_type']='VIDEOS';
            }
            else{
              $validateby="Posttype"; 
              $this->request->data['post_type']='POST';
            }
            
            $post = $this->Posts->patchEntity($post, $this->request->getData(),['validate' => $validateby]);

            if($post->errors()){
               echo json_encode(['result'=>'fail','data'=>$post->errors()]); 
            }
            else{
                if($this->Posts->save($post)) {
                    //$this->Flash->success(__('The post has been saved.'));
                  # create notifications
                  $this->Common->createNotification($this->Auth->user('id'), $this->Auth->user('id'), 'Create Twitt', $this->Auth->user('fullname').' has been tweet new content. ');

                  //  return $this->redirect(['action' => 'index']);
                }

            if($post->errors()){
                $this->Flash->error(__('The post could not be saved. Please, try again.'));
               echo json_encode(['result'=>'fail','data'=>$post->errors()]); 
            }
            else{
                echo json_encode(['result'=>'success','msg'=>'successfully saved.']);     
            }
        }//end if else
        exit;
      }//end ajax
            
        if ($this->request->is('post')) {
            $this->request->data['user_id']=$this->Auth->user('id');

            //start upload photos
           if(!empty($_FILES['media']) AND $this->request->data['post_type']=="PHOTOS"){
             
                $photos=$this->request->data['media'];
                $photosName = time().'_photos'.basename($photos["name"]);
                $targetDir = WWW_ROOT."/medias/";
                $targetFilePath = $targetDir . $photosName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('jpg','png','jpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($photos["tmp_name"], $targetFilePath)){
                        $this->request->data['media'] = $photosName;
                    }//end media
                }//end  
                //end upload media
          }//end media image

          //start upload videos
           if(!empty($_FILES['media']) AND $this->request->data['post_type']=="VIDEOS" ){
             
                $videos=$this->request->data['media'];
                $videosName = time().'_videos'.basename($videos["name"]);
                $targetDir = WWW_ROOT."medias/";
                $targetFilePath = $targetDir . $videosName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('mp4','mp3','mpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($videos["tmp_name"], $targetFilePath)){
                        $this->request->data['media'] = $videosName;
                    }//end videos
                }//end  
                //end upload videos
          }//end videos image

            if($this->request->data['post_type']=="POST"){
               $validateby="Posttype"; 
            }
            else if($this->request->data['post_type']=="PHOTOS"){
               $validateby="Photostype"; 
            }
            else if($this->request->data['post_type']=="VIDEOS"){
               $validateby="Videostype"; 
            }
            
            $post = $this->Posts->patchEntity($post, $this->request->getData(),['validate' => $validateby]);
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users','model'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success(__('The post has been deleted.'));
        } else {
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
