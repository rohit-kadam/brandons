<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FeatureModels Controller
 *
 * @property \App\Model\Table\FeatureModelsTable $FeatureModels
 *
 * @method \App\Model\Entity\FeatureModel[] paginate($object = null, array $settings = [])
 */
class FeatureModelsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Users');
        $this->paginate = [
        'contain' => ['Posts'=>['Likes']],
        'conditions'=>['Users.role_id'=> 2,'Users.status'=>'Active'],
        'order'=>['Users.modified'=>'DESC']   
        ];
        $featureModels = $this->paginate($this->Users);

        $this->set(compact('featureModels'));
        $this->set('_serialize', ['featureModels']);
    }

    /**
     * View method
     *
     * @param string|null $id Feature Model id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $featureModel = $this->FeatureModels->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('featureModel', $featureModel);
        $this->set('_serialize', ['featureModel']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $featureModel = $this->FeatureModels->newEntity();
        if ($this->request->is('post')) {
            $featureModel = $this->FeatureModels->patchEntity($featureModel, $this->request->getData());
            if ($this->FeatureModels->save($featureModel)) {
                $this->Flash->success(__('The feature model has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feature model could not be saved. Please, try again.'));
        }
        $users = $this->FeatureModels->Users->find('list', ['limit' => 200]);
        $this->set(compact('featureModel', 'users'));
        $this->set('_serialize', ['featureModel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Feature Model id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $featureModel = $this->FeatureModels->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $featureModel = $this->FeatureModels->patchEntity($featureModel, $this->request->getData());
            if ($this->FeatureModels->save($featureModel)) {
                $this->Flash->success(__('The feature model has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feature model could not be saved. Please, try again.'));
        }
        $users = $this->FeatureModels->Users->find('list', ['limit' => 200]);
        $this->set(compact('featureModel', 'users'));
        $this->set('_serialize', ['featureModel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Feature Model id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $featureModel = $this->FeatureModels->get($id);
        if ($this->FeatureModels->delete($featureModel)) {
            $this->Flash->success(__('The feature model has been deleted.'));
        } else {
            $this->Flash->error(__('The feature model could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
