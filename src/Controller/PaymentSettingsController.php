<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PaymentSettings Controller
 *
 *
 * @method \App\Model\Entity\PaymentSetting[] paginate($object = null, array $settings = [])
 */
class PaymentSettingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $paymentSettings = $this->paginate($this->PaymentSettings);

        $this->set(compact('paymentSettings'));
        $this->set('_serialize', ['paymentSettings']);
    }

    /**
     * View method
     *
     * @param string|null $id Payment Setting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentSetting = $this->PaymentSettings->get($id, [
            'contain' => []
        ]);

        $this->set('paymentSetting', $paymentSetting);
        $this->set('_serialize', ['paymentSetting']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
//        $paymentSetting = $this->PaymentSettings->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['expiration_date']=date("Y-m-d",strtotime($this->request->data['expiration_date']));
            $paymentSetting = $this->PaymentSettings->findByUserId($this->Auth->user('id'))->first();
            $this->request->data['user_id']=$this->Auth->user('id');
            if(count($paymentSetting)==0){
                $paymentSetting = $this->PaymentSettings->newEntity();    
            }

            $paymentSetting = $this->PaymentSettings->patchEntity($paymentSetting, $this->request->getData());
//            debug($paymentSetting); exit;
            if ($this->PaymentSettings->save($paymentSetting)) {
                $this->Flash->success(__('The payment setting has been saved.'));
                return $this->redirect(['controller'=>'users','action' => 'setting']);
            }
            $this->Flash->error(__('The payment setting could not be saved. Please, try again.'));
        }
              return $this->redirect(['controller'=>'users','action' => 'setting']);
        $this->set(compact('paymentSetting'));
        $this->set('_serialize', ['paymentSetting']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment Setting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paymentSetting = $this->PaymentSettings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $paymentSetting = $this->PaymentSettings->patchEntity($paymentSetting, $this->request->getData());
            if ($this->PaymentSettings->save($paymentSetting)) {
                $this->Flash->success(__('The payment setting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payment setting could not be saved. Please, try again.'));
        }
        $this->set(compact('paymentSetting'));
        $this->set('_serialize', ['paymentSetting']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Payment Setting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentSetting = $this->PaymentSettings->get($id);
        if ($this->PaymentSettings->delete($paymentSetting)) {
            $this->Flash->success(__('The payment setting has been deleted.'));
        } else {
            $this->Flash->error(__('The payment setting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
