<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;


ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_execution_time', 0);
ini_set('set_time_limit', 0);

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
      public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add','verification','forgotpassword','reset','getnotifications']);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function globalnotification()
    {
      $this->loadModel('Users');
      if($this->request->is('post')){
        $Active_users = $this->Users->find('all')->where(['Users.role_id IN '=>[2,3],'is_feature_models'=>'Active'])->order(['modified'=>'DESC']);
       
       $message = $this->request->data['message'];
       $notification_type = 'Subscription';
       foreach ($Active_users as $key => $value) {
          $user_id = $this->Auth->user('id');
          $model_id = $value['id'];
          $this->Common->createNotification($user_id,$model_id,$notification_type,$message,0);
        }//end Active_users
        $this->Flash->success(__('Notification has been sent successfully.!!!'));
      }//end post
      return $this->redirect(['action' => 'index']);
    }//end globalnotification

    public function searchmodel()
    {
        $this->loadModel('Users');
        $this->loadModel('Subscriptions');

       
        $searchkey = $this->request->data['search_keyword'];// = 'actor';
        $usersdata = TableRegistry::get('Users');
      /* $searchinData = $usersdata->find('all',['contain'=>['UserMedias','Subscriptions']])->where(['role_id' => 2, 'OR' => [['username LIKE' =>'%'.$searchkey.'%'], ['mykeywords LIKE' => '%'.$searchkey.'%']]]);*/
      $followers=$this->Subscriptions->find('all')->where(['user_id'=>$this->Auth->user('id')]);

      if($followers->count()>0){
        $follower_idArr= array_map(function($e1){ return $e1['follower_id']; },$followers->toArray()); 

        $searchinData = $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.id NOT IN '=>$follower_idArr,'Users.role_id' => 2,'OR' => [['Users.username LIKE' =>'%'.$searchkey.'%'], ['Users.mykeywords LIKE' => '%'.$searchkey.'%']]]);
      }
      else{
        $searchinData = $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id' => 2,'OR' => [['Users.username LIKE' =>'%'.$searchkey.'%'], ['Users.mykeywords LIKE' => '%'.$searchkey.'%']]]);
      }
      

/* pr($searchinData->all());
   exit;*/
          $this->set(compact('searchinData'));
          $this->set('_serialize', ['searchinData']);  
    
   
    }
    public function index()
    {
        $this->loadModel('Posts');
        $this->loadModel('Subscriptions');
        $post = $this->Posts->newEntity();

        if($this->Auth->user('role_id')==2){
          //redirect to model dashboard
           $model= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.id'=>$this->Auth->user('id')])->first(); 
           # get all follower user id
           $follwersId= $this->Subscriptions->find('all')->where(['user_id'=>$this->Auth->user('id')]);
           $follower_id_arr=array_map(function($e1){ return $e1['follower_id']; }, $follwersId->toArray());

           $liveusers=array();
           # live models 
           if(!empty($follower_id_arr)){
            $liveusers= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>3,'Users.status'=>'Active','Users.id IN '=>$follower_id_arr , 'Users.is_live'=>'Online'])->order(['Users.created'=>'DESC']);  
           }

           $follower_id_arr[]= $this->Auth->user('id');
           
           $models= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>2,'Users.status'=>'Active','Users.id NOT IN '=>$follower_id_arr])->order(['Users.created'=>'DESC']); 

          // pr($model); exit;
           $posts=$this->Posts->find('all',['contain' => ['Users'=>['UserMedias'], 'Comments'=>['Users'=>['UserMedias']], 'Likes']])->where(['Posts.status'=>'Active','Posts.user_id IN'=>$follower_id_arr])->order(['Posts.modified'=>'DESC']);
           # count posts, photos, videos
           $count_post= $this->Posts->find('all',['contain' => ['Users', 'Comments', 'Likes']])->where(['Posts.user_id'=>$this->Auth->user('id'),'Posts.post_type'=>'POST'])->count();
           $count_photos= $this->Posts->find('all',['contain' => ['Users', 'Comments', 'Likes']])->where(['Posts.user_id'=>$this->Auth->user('id'),'Posts.post_type'=>'PHOTOS'])->count();
           $count_videos= $this->Posts->find('all',['contain' => ['Users', 'Comments', 'Likes']])->where(['Posts.user_id'=>$this->Auth->user('id'),'Posts.post_type'=>'VIDEOS'])->count();


           $this->set(compact('model','post','posts','count_post','count_photos','count_videos','models','liveusers'));
           $this->render('model_user_dashboard');
        }
        else if($this->Auth->user('role_id')==3){
          //redirect to normal user dashboard
           $model= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.id'=>$this->Auth->user('id')])->first(); 

           # get all follower user id
           $follwersId= $this->Subscriptions->find('all')->where(['user_id'=>$this->Auth->user('id')]);
           $follower_id_arr=array_map(function($e1){ return $e1['follower_id']; }, $follwersId->toArray());
            
           $liveusers=array(); 
           # live models 
           if(!empty($follower_id_arr)){
            $liveusers= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>2,'Users.status'=>'Active','Users.id IN '=>$follower_id_arr , 'Users.is_live'=>'Online'])->order(['Users.created'=>'DESC']);  
           }

           $follower_id_arr[]= $this->Auth->user('id');

           $models= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>2,'Users.status'=>'Active','Users.id NOT IN '=>$follower_id_arr])->order(['Users.created'=>'DESC']); 
           
           if(!empty($this->Auth->user('intrestedkeywords'))){
            $intrested= explode(",",$this->Auth->user('intrestedkeywords'));
            $recommended_users = $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>2,'Users.status'=>'Active','Users.id NOT IN '=>$follower_id_arr,'Users.mykeywords IN'=>$intrested])->order(['Users.created'=>'DESC']);  
           }
           else{
            $recommended_users = $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>2,'Users.status'=>'Active','Users.id NOT IN '=>$follower_id_arr])->order(['Users.created'=>'DESC']); 
           }
           
           $posts=$this->Posts->find('all',['contain' => ['Users'=>['UserMedias'], 'Comments'=>['Users'=>['UserMedias']], 'Likes']])->where(['Posts.status'=>'Active','Posts.user_id IN'=>$follower_id_arr])->order(['Posts.modified'=>'DESC']);
           $this->set(compact('posts','post','model','models','liveusers','recommended_users'));
           $this->render('normal_user_dashboard');
        }
        else{
          // redirect to admin dashboard
          $this->loadModel('Users');
          $users = $this->paginate($this->Users,['contain'=>['Roles'],'limit'=>10,'conditions'=>['Users.role_id IN '=>[2,3]],'order'=>['modified'=>'DESC']]);
          $total_register_member =$this->Users->find('all')->where(['Users.role_id IN '=>[2,3]])->order(['modified'=>'DESC'])->count();
          $modelcount = $this->Users->find('all',['contain'=>['Roles']])->where(['Users.role_id IN '=>[2]])->order(['modified' => 'DESC'])->count();

           $usercount = $this->Users->find('all',['contain'=>['Roles']])->where(['Users.role_id IN '=>[3]])->order(['modified' => 'DESC'])->count();
           $Active_models =$this->Users->find('all',['contain'=>['Roles']])->where(['status'=>'Active','Users.role_id IN '=>[2]])->order(['modified' => 'DESC'])->count();
           $Active_Users = $this->Users->find('all',['contain'=>['Roles']])->where(['status'=>'Active','Users.role_id IN '=>[3]])->order(['modified' => 'DESC'])->count();

          $this->set(compact('users','modelcount','usercount','total_register_member','Active_models','Active_Users'));
          $this->set('_serialize', ['users']);  
        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         $this->loadModel('UserMedias');   
         $this->viewBuilder()->layout('login');
         $user = $this->Users->newEntity(); 
        
        if ($this->request->is('post')) {
            //start upload pics
           if(!empty($_FILES['pics'])){
             
                $profile=$this->request->data['pics'];
                $profileName = time().'_pics'.basename($profile["name"]);
                $targetDir = WWW_ROOT."img/profile/";
                $targetFilePath = $targetDir . $profileName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('jpg','png','jpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($profile["tmp_name"], $targetFilePath)){
                        $this->request->data['pics'] = $profileName;
                    }//end pics
                }//end  
                //end upload pics
          }//end pics image

          //start upload body_pics
           if(!empty($_FILES['body_pics'])){
             
                $body_pics=$this->request->data['body_pics'];
                $body_picsName = time().'_pics'.basename($body_pics["name"]);
                $targetDir = WWW_ROOT."img/profile/";
                $targetFilePath = $targetDir . $body_picsName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('jpg','png','jpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($body_pics["tmp_name"], $targetFilePath)){
                        $this->request->data['body_pics'] = $body_picsName;
                    }//end body_pics
                }//end  
                //end upload body_pics
          }//end body_pics image

          //start upload face_pics
           if(!empty($_FILES['face_pics'])){
             
                $face_pics=$this->request->data['face_pics'];
                $face_picsName = time().'_pics'.basename($face_pics["name"]);
                $targetDir = WWW_ROOT."img/profile/";
                $targetFilePath = $targetDir . $face_picsName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('jpg','png','jpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($face_pics["tmp_name"], $targetFilePath)){
                        $this->request->data['face_pics'] = $face_picsName;
                    }//end face_pics
                }//end  
                //end upload face_pics
          }//end face_pics image

          //start upload videos
           if(!empty($_FILES['videos'])){
             
                $videos=$this->request->data['videos'];
                $videosName = time().'_pics'.basename($videos["name"]);
                $targetDir = WWW_ROOT."videos/";
                $targetFilePath = $targetDir . $videosName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('mp4','mp3','mpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($videos["tmp_name"], $targetFilePath)){
                        $this->request->data['videos'] = $videosName;
                    }//end videos
                }//end  
                //end upload videos
          }//end videos image

          //start upload id_proof
           if(!empty($_FILES['id_proof'])){
             
                $id_proof=$this->request->data['id_proof'];
                $id_proofName = time().'_pics'.basename($id_proof["name"]);
                $targetDir = WWW_ROOT."img/id_proof/";
                $targetFilePath = $targetDir . $id_proofName;
                
                //allow certain file formats
                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                $allowTypes = array('jpg','png','jpeg');
                if(in_array(strtolower($fileType), $allowTypes)){
                    //upload file to server
                    if(move_uploaded_file($id_proof["tmp_name"], $targetFilePath)){
                        $this->request->data['id_proof'] = $id_proofName;
                    }//end id_proof
                }//end  
                //end upload id_proof
          }//end id_proof image

          $this->request->data['role_id'] =2;
          $this->request->data['verification_code']=Text::uuid();
          $this->request->data['referrer_id']=$this->generateRandomString();

            $user = $this->Users->patchEntity($user, $this->request->data,['validate' => 'Register']);
//            debug($user); exit;

            if($this->Users->save($user)){
                $userMedia = $this->UserMedias->newEntity(); 
                $this->request->data['user_id']=$user->id;
                $userMedia = $this->UserMedias->patchEntity($userMedia,$this->request->getData());                
                if(!$this->UserMedias->save($userMedia)) {
                    $this->Flash->success(__(' Media files are not uploaded.'));
                }    

                //send email
                $this->Common->get_email_template_registered($this->request->data['email'], $this->request->data['password'],'Model',$this->request->data['verification_code']);

                //$this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    public function editprofile($id = null)
    {
        $this->loadModel('UserMedias');
        
        $user = $this->Users->get($id, [
            'contain' => ['UserMedias']
        ]);

        if($this->request->is(['patch', 'post', 'put'])) {


                $userMedia = $this->UserMedias->find('all')->where(['user_id'=>$this->Auth->user('id')])->first(); 

                //start upload pics
                   if(!empty($_FILES['pics'])){
                     
                        $profile=$this->request->data['pics'];
                        $profileName = time().'_pics'.basename($profile["name"]);
                        $targetDir = WWW_ROOT."img/profile/";
                        $targetFilePath = $targetDir . $profileName;
                        
                        //allow certain file formats
                        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                        $allowTypes = array('jpg','png','jpeg');
                        if(in_array(strtolower($fileType), $allowTypes)){
                            //upload file to server
                            if(move_uploaded_file($profile["tmp_name"], $targetFilePath)){
                                //delete old file
                                $file = new File(WWW_ROOT.'img/profile/'.$userMedia->pics);
                                $contents = $file->delete();

                                //assign new update file
                               $userMedia->pics = $profileName;
                                
                            }//end pics
                        }//end  
                        //end upload pics
                  }//end pics image

                  //start upload body_pics
                   if(!empty($_FILES['body_pics'])){
                     
                        $body_pics=$this->request->data['body_pics'];
                        $body_picsName = time().'_pics'.basename($body_pics["name"]);
                        $targetDir = WWW_ROOT."img/profile/";
                        $targetFilePath = $targetDir . $body_picsName;
                        
                        //allow certain file formats
                        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                        $allowTypes = array('jpg','png','jpeg');
                        if(in_array(strtolower($fileType), $allowTypes)){
                            //upload file to server
                            if(move_uploaded_file($body_pics["tmp_name"], $targetFilePath)){

                                //delete old file
                                $file = new File(WWW_ROOT.'img/profile/'.$userMedia->body_pics);
                                $contents = $file->delete();

                                //assign new update file 
                                $userMedia->body_pics = $body_picsName;
                            }//end body_pics
                        }//end  
                        //end upload body_pics
                  }//end body_pics image

                  //start upload videos
                   if(!empty($_FILES['videos'])){
                     
                        $videos=$this->request->data['videos'];
                        $videosName = time().'_pics'.basename($videos["name"]);
                        $targetDir = WWW_ROOT."videos/";
                        $targetFilePath = $targetDir . $videosName;
                        
                        //allow certain file formats
                        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                        $allowTypes = array('mp4','mp3','mpeg');
                        if(in_array(strtolower($fileType), $allowTypes)){
                            //upload file to server
                            if(move_uploaded_file($videos["tmp_name"], $targetFilePath)){
                                //delete old file
                                $file = new File(WWW_ROOT.'videos/'.$userMedia->videos);
                                $contents = $file->delete();

                                //assign new file 
                               $userMedia->videos = $videosName;
                            }//end videos
                        }//end  
                        //end upload videos

                  }//end videos image

                  //start upload id_proof
                   if(!empty($_FILES['id_proof'])){
                     
                        $id_proof=$this->request->data['id_proof'];
                        $id_proofName = time().'_pics'.basename($id_proof["name"]);
                        $targetDir = WWW_ROOT."img/id_proof/";
                        $targetFilePath = $targetDir . $id_proofName;
                        
                        //allow certain file formats
                        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                        $allowTypes = array('jpg','png','jpeg');
                        if(in_array(strtolower($fileType), $allowTypes)){
                            //upload file to server
                            if(move_uploaded_file($id_proof["tmp_name"], $targetFilePath)){
                                //delete old file
                                $file = new File(WWW_ROOT.'id_proof/'.$userMedia->id_proof);
                                $contents = $file->delete();

                                //assign new file
                               $userMedia->id_proof = $id_proofName;
                            }//end id_proof
                        }//end  
                        //end upload id_proof
                  }//end id_proof image

                  if(!empty($this->request->data['model_attribute'])){
                    $userMedia->model_attribute = $this->request->data['model_attribute'];
                  }//end model_attribute

             
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                
                    if(!$this->UserMedias->save($userMedia)) {
                        $this->Flash->success(__(' Media files are not uploaded.'));
                    }    

                $this->Auth->setUser($user->toArray());

                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'editprofile',$id]);
            }
            
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }//end editprofile

     //update profilr image
    public function storeprofile(){
     $this->autoRender=False;  
     $Imagepath=$this->request->webroot.'img/profile/';
        $imagename = explode($Imagepath,$this->request->data['tempName']);
            $usersTable = TableRegistry::get('user_medias');
            $user = $usersTable->find('all')->where(['user_id'=>$this->Auth->user('id')])->first(); // Return article with id 12
            $user->face_pics =$imagename[1];
           if( $usersTable->save($user)){
            //$this->Auth->setUser($user->toArray());
                echo  json_encode(array('result'=>'successfully save file.'.$imagename[1]));  exit;
           }
           else{
            echo  json_encode(array('result'=>'Something went to wrong.'.$imagename[1]));  exit;
           }
    }//end storeprofile

    public function updatefeaturemodel($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if($user->is_feature_models=="Active"){
           $user->is_feature_models='Inactive';
        }
        else{
            $user->is_feature_models='Active';
        }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Featured model status updated successfully.'));
            }
            else{
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        return $this->redirect($this->referer());

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $id=$this->Auth->user('id');
        $user = $this->Users->get($id);
        $user->deleted_date= date("Y-m-d",strtotime("+1 days")); 

        if ($this->Users->save($user)) {
            $this->Flash->success(__('Your account will have deleted till '.$user->deleted_date));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'setting']);
    }

    public function login()
    {   
        $this->loadModel('UserMedias'); 
        $this->loadModel('Subscriptions'); 
        $this->viewBuilder()->layout('login');
        $user = $this->Users->newEntity();

         // if user already logged-in
        if ($this->Auth->user()){
            return $this->redirect($this->Auth->redirectUrl());
        }//end if user already logged in

        if($this->request->is('post')) {

            if(isset($this->request->data['login']) AND $this->request->data['login']==1){
                    //if email password is empty
                    if(empty($this->request->data['email']) || empty($this->request->data['password'])){
                        $this->Flash->error(__('Please enter your Email and Password.'));
                        return $this->redirect(['action'=>'login']);
                    }//end email password

                    $ch=curl_init();
                    curl_setopt($ch,CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
                    curl_setopt($ch,CURLOPT_POST,true);
                    curl_setopt($ch,CURLOPT_POSTFIELDS,array('secret'=>'6Lcd-DIUAAAAAJctgVJG37fNFmwaAa1ewiZUHzPa','response'=>$this->request->data['g-recaptcha-response']));
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                    $result=curl_exec($ch);
                    $response=json_decode($result,true); 
                    if(intval($response['success'])!==1){
                        $this->Flash->error(__('reCaptcha verification failed, Please try again'));
                        return $this->redirect(['action'=>'login']);
                    }

                    $user = $this->Auth->identify();
                    
                    if ($user) {
                        $this->Auth->setUser($user);
                        if($user['status']=="Inactive"){
                             $this->Flash->error(' Your account is not verified. please check in your email activation link. ');
                             return $this->redirect($this->Auth->logout());
                        }//end if account is Inactive redirect to login
                            return $this->redirect($this->Auth->redirectUrl());
                    }
                    $this->Flash->error(__('Your username or password is incorrect.'));
                    return $this->redirect(['action'=>'login']);
            }// only login
        }//end POST

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            
            $this->response->disableCache();
            $this->request->data['status']="Inactive";
            $this->request->data['role_id']=3;
            
            $this->request->data['verification_code']=Text::uuid();
            //$this->request->data['referrer_id']=$this->generateRandomString();

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if($user->errors()){
               echo json_encode(['result'=>'fail','data'=>$user->errors()]); 
            }
            else{
                    if($this->Users->save($user)) {
                       // $this->Flash->success(__('The user has been saved.'));
                        $userMedia = $this->UserMedias->newEntity(); 
                        $this->request->data['user_id']=$user->id;
                        $this->request->data['id_proof']='1508756764_picsbank.png';
                        $this->request->data['pics']='default-pics.png';
                        $this->request->data['body_pics']='1508233997_pics04-operative.jpg';
                        $this->request->data['face_pics']='bc-user-default.png';
                        $this->request->data['videos']='1508233997_picsSampleVideo_1280x720_1mb.mp4';
                        $this->request->data['model_attribute']='User';
                        $this->request->data['status']='Active';
                        $userMedia = $this->UserMedias->patchEntity($userMedia,$this->request->getData());                
                        if(!$this->UserMedias->save($userMedia)) {
                            $this->Flash->success(__(' Media files are not uploaded.'));
                        }
                        //send email
                        $this->Common->get_email_template_registered($this->request->data['email'], $this->request->data['password'],'User ',$this->request->data['verification_code']);

                    }
                    if($user->errors()){
                       echo json_encode(['result'=>'fail','data'=>$user->errors()]); 
                    }
                    else{
                        echo json_encode(['result'=>'success','msg'=>'successfully saved.']);     
                    }
                    
            }//end if else 

        }//end request ajax

        $featureModels= $this->Users->find('all',['contain'=>['UserMedias']])->where(['status'=>'Active','is_feature_models'=>'Active'])->order(['modified'=>'DESC']);
        $Subscriptions= $this->Subscriptions->find('all',['contain'=>['Users']]);
        $subscribedUsers=array_map(function($e1){ return $e1['follower_id']; },$Subscriptions->toArray()); 
        $subscribedUsers= array_count_values($subscribedUsers);
        $this->set(compact('user','featureModels','subscribedUsers'));
    }//end login action

      public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }


     
   public function changepassword(){
        $users=$this->Users->get($this->Auth->user('id'));

        if($this->request->is('POST')){
            if(trim($this->request->data['password'])=="" || trim($this->request->data['confirmpassword'])==""){
                $this->Flash->error(__('Please enter password and confirm paswword'));
                return $this->redirect(['action'=>'changepassword']);
            }
            elseif(trim($this->request->data['password'])!=trim($this->request->data['confirmpassword'])){
               $this->Flash->error('Password does not match the confirm password.');
                 return $this->redirect(['action'=>'changepassword']);
            }

           $users= $this->Users->patchEntity($users,$this->request->data);
           if($this->Users->save($users)){
                $this->Flash->success(__(' Password has been updated successfully. '));
           }//end save password
        }//end request
        $this->set(compact('users'));
    }//end changepassword
   

    public function forgotpassword(){
       $this->viewBuilder()->layout('login');
        if($this->request->is('post')) {
             //if email password is empty
            if(empty($this->request->data['email'])){
                $this->Flash->error('Please enter your registred email.');
                return $this->redirect(['controller'=>'Users','action'=>'forgotpassword']);
            }//end email password

            //Check email is valid not valid
            if (filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL) === false) {
              $this->Flash->error($this->request->data['email']." is not a valid email address");
                return $this->redirect(['controller'=>'Users','action'=>'forgotpassword']);
            }//end check valid email id

            $u=$this->Users->find('all',['fields'=>'id'])->where(array('email'=>$this->request->data['email']));
            if($u->count()>0){
                // if email exist
                $u=$u->first();
                $users = $this->Users->get($u->id);
                $users->verification_code=Text::uuid();
                $users = $this->Users->patchEntity($users, $this->request->data);
                if ($this->Users->save($users)) {

                    $message ='<html> <head> <title>'.Configure::read('projectTitle').' | Reset Password</title> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1"> <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <style type="text/css"> body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; } table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; } img { -ms-interpolation-mode: bicubic; } img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; } table { border-collapse: collapse !important; } body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; } a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; } @media screen and (max-width: 480px) { .mobile-hide { display: none !important; } .mobile-center { text-align: center !important; } .mobile-table { width: 100% !important; } .text-center { text-align: center !important; } } div[style*="margin: 16px 0;"] { margin: 0 !important; } </style> </head> <body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee"> <table style="border:2px solid #ef4e23;margin:20px;" align="center" border="0" cellpadding="0" cellspacing="0" width="768" class="mobile-table"> <tr> <td align="center" valign="top" style="font-size:0; padding: 35px 35px 0px 35px;" bgcolor="#ffffff"> <img width="120" src="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'img/logo.png" /> </td> </tr>';
                    $message .=' <tr> <td align="center" style="padding: 0px 15px 20px 15px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 0px;"> <br> <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin:0px;" mc:edit="Headline"> Hello,<br>Welcome to '.Configure::read('projectTitle').' </h2> </td> </tr>';
                    $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 20px; padding-top:0px;" mc:edit="Paragraph"> <p style="text-align:center;line-height: 0px; color: #777777;"> Click on the below link to reset password. </p> </td> </tr>';
                    $message .='<tr> <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 16px; padding-top: 0px;" mc:edit="Paragraph"> <p style="text-align:left;font-size: 16px;font-weight: 400;line-height: 16px;color: #777777;margin-top:50px;text-align:center;"> <a style="text-decoration:none;padding:12px 10px;background:#EF4E23; color:#fff; border-color:#EF4E23;border-radius:4px; white-space:nowrap;" href="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'Users/reset/'.$users->verification_code.'" target="_blank">Reset password</a></p> </td> </tr>';
                    $message .=' </table></td> </tr> <tr> <td align="center" style="padding:0px 35px; background-color: #ffffff;" bgcolor="#ffffff"> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">  <tr> <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 0px 0 10px 0;" mc:edit="Address"> <p style="font-size: 14px; font-weight: 800; line-height:0px; color: #333333;"> <strong>Copyright &copy; '.date('Y').' <a href="http://'.$_SERVER['SERVER_NAME'].$this->request->webroot.'">'.Configure::read('projectTitle').'</a>.</strong> All rights reserved. </p> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </body> </html>';

                    $email = new Email('default');

                       if($email->from([Configure::read('from_email') => Configure::read('projectTitle').' | Reset Password'])
                        ->emailFormat('html')
                        ->to($this->request->data['email'])
                        ->subject(Configure::read('projectTitle').' | Reset Password')
                        ->send($message))
                         {
                             $this->Flash->success(' Email has been sent on your email.');    
                               return $this->redirect(['controller'=>'Users','action'=>'login']);
                        } 
                         else{
                               $this->Flash->error('Email is not sent.');                 
                         }  
                }
                
             }
             else{
                // if email does not exist
                $this->Flash->error(' This email is not registered. ');   
             }   

        }//end POST
    }//end post 

    function reset($token=null){
        //$this->layout="Login";
        $this->viewBuilder()->layout('login');

        //$this->User->recursive=-1;
        if(!empty($token)){
            //$u=$this->Users->findByverification_code($token);
            $u=$this->Users->find('all',['fields'=>'id'])->where(array('verification_code'=>$token,'status'=>'Active'));
            if($u->count()>0){
                $u=$u->first();
               $this->Users->id=$u->id;
                if($this->request->is('post'))
                {

                    $this->request->data['id']=$u->id;
                    if(empty($this->request->data['password']))
                    {
                        $this->Flash->error('Please enter password.');
                        return $this->redirect(['controller'=>'Users','action'=>'reset',$token]);
                    }//end email password

                    if(strlen($this->request->data['password'])<6 || strlen($this->request->data['password'])>15)
                    {
                        $this->Flash->error('Required password between 6 to 15 charachter long.');
                        return $this->redirect(['controller'=>'Users','action'=>'reset',$token]);
                    }//end Password minimum 6 charachter long

                    if(empty($this->request->data['password_confirm']))
                    {
                        $this->Flash->error('Please enter confirm password.');
                        return $this->redirect(['controller'=>'Users','action'=>'reset',$token]);
                    }//end  password_confirm
                    
                    if(strtolower($this->request->data['password'])!=strtolower($this->request->data['password_confirm']))
                    {
                        $this->Flash->error('Password does not match the confirm password.');
                        return $this->redirect(['controller'=>'Users','action'=>'reset',$token]);
                    }//end Password and Confirm password does not match


                    $users = $this->Users->get($u->id);

                    if($this->request->is(['patch', 'post', 'put']))
                    {

                        if($this->request->data['password']==$this->request->data['password_confirm'])
                        {
                            // if password and password_confirm are same
                            $this->request->data['verification_code']="";
                            $users = $this->Users->patchEntity($users, $this->request->data);
                            if ($this->Users->save($users)) {
                                $this->Flash->success(' Password Has been Updated ');
                                return $this->redirect(['controller'=>'Users','action' => 'login']);
                            }
                            $this->Flash->error(' Something went wrong. Please, try again. ');    
                        }
                        else{
                            $this->Flash->error('Password does not match the confirm password.');     
                        }
                        
                    }//end post
                }
            }
            else
            {
                 $this->Flash->error('Token Corrupted,Please Retry.the reset link work only for once.'); 
                  return $this->redirect(['controller'=>'Users','action' => 'login']);
            }
        }
        else{
            $this->redirect('/');
        }
        $this->set(compact('token'));
    } 

# verfication link
  public function verification($verification_code=null){
      if(isset($verification_code)){
          $getUser=$this->Users->find('all')->where(['verification_code'=>$verification_code])->first();
          if(count($getUser)>0){
              if($getUser->status=="Active"){
                  $this->Flash->warning(__(' Your account is already activated. '));
                  return $this->redirect($this->Auth->redirectUrl());
              }

              $this->request->data['status']="Active";
              $this->request->data['verification_code']="";
              $getUser= $this->Users->patchEntity($getUser,$this->request->data);
              if($this->Users->save($getUser)){
                  $this->Flash->success(__(' Your account is activated. '));
                  return $this->redirect($this->Auth->redirectUrl()); 
              }
          }//end 
          else{
                 $this->Flash->error(__(' Your verification code is expired or does not exist. '));
               return $this->redirect($this->Auth->redirectUrl());
          }

      }//end verification_code
  }//end verfication

  public function generateRandomString($length = 5) {
      $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }

  public function sample(){
    echo $this->Common->get_email_template_registered('dnyaneshwar@bitwaretechnologies.com', 'admin123','Model','DANY123');
    exit;
  }

  public function model()
  {
    $models= $this->Users->find('all',['contain'=>['UserMedias']])->where(['Users.role_id'=>2,'Users.status'=>'Active']); 

    $models= $this->paginate($models);
    $this->set(compact('models'));
  }//end model


  public function fearuredmodel()
    {
        $this->paginate = [
        'contain' => ['Posts'=>['Likes']],
        'conditions'=>['Users.role_id'=> 2,'Users.status'=>'Active'],
        'order'=>['Users.modified'=>'DESC']   
        ];
        $featureModels = $this->paginate($this->Users);

        $this->set(compact('featureModels'));
        $this->set('_serialize', ['featureModels']);
    }

    public function setting(){
      $this->loadModel('EmailNotifications');
      $this->loadModel('PaymentSettings');
      $this->loadModel('Banks');
      $this->loadModel('Subscriptions');
      $getDeleted_date=$user= $this->Users->get($this->Auth->user('id'));
      $getDeleted_date=$getDeleted_date->deleted_date;
      $notifications= $this->EmailNotifications->find('all',['fields'=>['subsriber_type','status']])->where(['user_id'=>$this->Auth->user('id')]);
      $notificationsType= array_map(function($e1){ return $e1['subsriber_type']; },$notifications->toArray());
      $notificationsStatus= array_map(function($e1){ return ($e1['status']=="On"?"checked":"unchecked"); },$notifications->toArray());
      $followings= $this->Subscriptions->find('all',['contain'=>['Users']])->where(['Subscriptions.follower_id'=>$this->Auth->user('id')])->order(['Subscriptions.created'=>'DESC']);

      # banks
      $bank = $this->Banks->findByUserId($this->Auth->user('id'))->first();
      if(count($bank)==0){
          $bank = $this->Banks->newEntity();    
      }

      # payment setting
       $paymentSetting = $this->PaymentSettings->findByUserId($this->Auth->user('id'))->first();
        if(count($paymentSetting)==0){
            $paymentSetting = $this->PaymentSettings->newEntity();    
        }

      $toggleNotify=[];
      if(!empty($notificationsType)){
        $toggleNotify=array_combine($notificationsType, $notificationsStatus);
      }
      $this->set(compact('getDeleted_date','toggleNotify','user','followings','bank','paymentSetting'));
    }//end setting

    public function getnotifications(){

      $this->loadModel('EmailNotifications');
      $this->loadModel('TweetNotification'); 
      $this->loadModel('Notifications');
      $this->loadModel('Subscriptions');
      
      $createArr=[];

      $services= $this->EmailNotifications->find('all',['fields'=>['subsriber_type']])->where(['user_id'=>$this->Auth->user('id'),'status'=>'On']);
    
      if($services->count()>0){
        $subsriber_type= array_map(function($e1){ return $e1['subsriber_type']; }, $services->toArray());

        if($this->Auth->user('role_id')==2){
          $notifications= $this->Notifications->find('all')->where(['model_id'=>$this->Auth->user('id'),'status'=>'Unread']);  

        }
        else{
          # get all follower user id
           $follwersId= $this->Subscriptions->find('all')->where(['user_id'=>$this->Auth->user('id')]);
           $follower_id_arr=array_map(function($e1){ return $e1['follower_id']; }, $follwersId->toArray());
          //ss $follower_id_arr[]= $this->Auth->user('id');
         /*   pr($follower_id_arr);
      exit;*/
           if(!empty($follower_id_arr)){
              $notifications= $this->Notifications->find('all')->where(['user_id IN '=>$follower_id_arr,'status'=>'Unread']);    
           }//end NOT empty
           else{
            echo json_encode(['result'=>'success','data'=>$createArr]); exit;
           }
        }


        if($notifications->count()>0){
          foreach ($notifications as $notify) {
             $createArr[]=[
              'id'=> $notify['id'],
              'type'=> $notify['notification_type'],
              'message'=> $notify['message']  
              ]; 
          }//end foreach
        }//end notifications
        echo json_encode(['result'=>'success','data'=>$createArr]); exit;
      }//end services

      echo json_encode(['result'=>'success','data'=>$createArr]); exit;
    }//end getnotifications

    // read notification message
   public function readnotification(){
    $this->loadModel('Notifications');
    $notification= $this->Notifications->get($this->request->data['id']);
    $notification->status= "Read";
    $notification= $this->Notifications->save($notification);
      echo json_encode(['result'=>'success']); exit;
   }//end readnotification

   public function togglenotification(){
    $this->loadModel('EmailNotifications');
    $this->loadModel('TweetNotification'); 

      if($this->request->is('post')){
          $togglenotification=[
            'newsubscription'=> 'New Subscriber',
            'subscriptionprolong'=>'Subscriber Prolong' ,
            'newtip' =>'New Tip',
            'newprivatemessage'=>'New Private Message',
            'newreferral' =>'New Referral',
            'newpostemail' =>'New post email summery'
            ];

            $bindData=[
              'user_id'=> $this->Auth->user('id'),
              'subsriber_type'=> $togglenotification[$this->request->data['name']],
               'status'=>'On'               
              ];

            $checkexist= $this->EmailNotifications->find('all')->where(['user_id'=>$this->Auth->user('id'), 'subsriber_type'=> $togglenotification[$this->request->data['name']] ])->first();  
            if(count($checkexist)>0){
              // if notification already exist
                   $bindData=[
                   'status'=>($checkexist->status=="On"?"Off":"On")             
                  ];
                  $checkexist = $this->EmailNotifications->patchEntity($checkexist, $bindData);
                  $checkexist = $this->EmailNotifications->save($checkexist);
                 echo  json_encode(['result'=>'success']); exit;

            }
            else{
              // add new notification
              $notification =$this->EmailNotifications->newEntity();
              $notification = $this->EmailNotifications->patchEntity($notification, $bindData);
              $notification = $this->EmailNotifications->save($notification);
              echo  json_encode(['result'=>'success']); exit; 
            }
      }//end 
   }//end togglenotifications

   public function updatepassword(){
    $user=$this->Users->get($this->Auth->user('id'));

    if($this->request->is('post')){
      $this->request->data['confirmpassword']=$this->request->data['password'];
      $user= $this->Users->patchEntity($user, $this->request->data);
      if($user->errors()){
        $this->Flash->error(' Something went wrong, Please try again. ');
        return $this->redirect(['action'=>'setting']);
      }

      if($this->Users->save($user)){
        $this->Flash->success(__(' successfully updated password. '));
        return $this->redirect(['action'=>'setting']);
      }
    }//end POST
      $this->Flash->error(__(' Please enter password'));
    return $this->redirect(['action'=>'setting']);
   }//end changepasword

   public function upateamount(){
    $this->loadModel('Subscriptions');
    
   $user=$this->Users->get($this->Auth->user('id'));
    if($this->request->is('post')){
      $user= $this->Users->patchEntity($user, $this->request->data);
      if($user->errors()){
        $this->Flash->error(' Something went wrong, Please try again. ');
        return $this->redirect(['action'=>'setting']);
      }

      if($this->Users->save($user)){

        # get all followers Ids
        $followers= $this->Subscriptions->find('all')->where(['Subscriptions.user_id'=>$this->Auth->user('id')]);
        if($followers->count()>0){
          $follwersId=array_map(function($e1){ return $e1->follower_id; },$followers->toArray());
          foreach ($follwersId as $model_id) {
            $this->Common->createNotification($this->Auth->user('id'), $model_id, 'Price Changed', $this->Auth->user('full_name').' model has been changed price as '.$this->request->data['amount'] );
          }
        }//end followers

        $this->Flash->success(__(' successfully updated amount. '));
        return $this->redirect(['action'=>'setting']);
      }
    }//end POST
      $this->Flash->error(__(' Please enter amount'));
    return $this->redirect(['action'=>'setting']);
   }//end upateamount

   public function updatekeywords(){
      $user=$this->Users->get($this->Auth->user('id'));
      if($this->request->is('post')){
        $user= $this->Users->patchEntity($user, $this->request->data);
        if($user->errors()){
          $this->Flash->error(' Something went wrong, Please try again. ');
          return $this->redirect(['action'=>'setting']);
        }

        if($this->Users->save($user)){
          $this->Auth->setUser($user->toArray());
          $this->Flash->success(__(' successfully updated keywords. '));
          return $this->redirect(['action'=>'setting']);
        }
      }//end POST
        $this->Flash->error(__(' Please enter keywords. '));
      return $this->redirect(['action'=>'setting']);
   }//end updatekeywords

   # myfeed
   public function myfeed(){
    $this->loadModel('Subscriptions');
    $this->loadModel('Posts');
    $posts=$this->Posts->find('all',['contain' => ['Users'=>['UserMedias'], 'Comments'=>['Users'=>['UserMedias']], 'Likes']])->where(['Posts.status'=>'Active','Posts.user_id'=>$this->Auth->user('id')])->order(['Posts.modified'=>'DESC']);
    $this->set(compact('posts'));
   }//end myfeed

   # normalfeed
   public function normalfeed(){
     $this->loadModel('Subscriptions');
     $this->loadModel('Posts');

     # get all follower user id
     $follwersId= $this->Subscriptions->find('all')->where(['user_id'=>$this->Auth->user('id')]);
     $follower_id_arr=array_map(function($e1){ return $e1['follower_id']; }, $follwersId->toArray());

     $posts=$this->Posts->find('all',['contain' => ['Users'=>['UserMedias'], 'Comments'=>['Users'=>['UserMedias']], 'Likes']])->where(['Posts.status'=>'Active','Posts.user_id IN'=>$follower_id_arr])->order(['Posts.modified'=>'DESC']);
     $this->set(compact('posts'));
   }//end normalfeed

   public function getprofile(){

    echo 'im hwerer '.$this->request->username; exit;
   }
}
