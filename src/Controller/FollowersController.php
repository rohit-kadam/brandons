<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Number;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[] paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $followers = $this->paginate($this->Followers);

        $this->set(compact('followers'));
        $this->set('_serialize', ['followers']);
    }

    /**
     * View method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => ['Users', 'Followers', 'Subscriptions']
        ]);

        $this->set('follower', $follower);
        $this->set('_serialize', ['follower']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($follower_id=null)
    {
        $follower = $this->Followers->newEntity();
        $followerData= [
            'user_id' => $this->Auth->user('id'),
            'follower_id' => $follower_id,
            'status'=>'Active' 
        ];

            $follower = $this->Followers->patchEntity($follower, $followerData);
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('You have successfully followed to model.'));
            }
            else{
                $this->Flash->error(__('Something went wrong .'));
            }
                        debug($follower); exit;

         return $this->redirect($this->referer());

    }

    /**
     * Edit method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $users = $this->Followers->Users->find('list', ['limit' => 200]);
        $this->set(compact('follower', 'users'));
        $this->set('_serialize', ['follower']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $follower = $this->Followers->get($id);
        if ($this->Followers->delete($follower)) {
            $this->Flash->success(__('The follower has been deleted.'));
        } else {
            $this->Flash->error(__('The follower could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function follow()
    {
        $this->loadModel('Users');
        $this->loadModel('Posts');
        $this->loadModel('Likes');

        $model= $this->Users->findByUsername($this->request->username)->contain(['UserMedias'])->first();
        //pr($model); exit;
        if(count($model)==0){
            $this->Flash->error(__(' Username does not exit. '));
            return $this->redirect(['controller'=>'users','action'=>'index']);
        }

        $postsCount= $this->Posts->find('all')->where(['post_type'=>'POST','user_id'=>$model->id,'status'=>'Active'])->count();
        $photosCount= $this->Posts->find('all')->where(['post_type'=>'PHOTOS','user_id'=>$model->id,'status'=>'Active'])->count();
        $videosCount= $this->Posts->find('all')->where(['post_type'=>'VIDEOS','user_id'=>$model->id,'status'=>'Active'])->count();

        $postsId= $this->Posts->find('all')->where(['user_id'=>$model->id,'status'=>'Active']);
        $postsId= array_filter(array_map(function($e1){ return $e1['id']; }, $postsId->toArray())) ;
        if(!empty($postsId)){
            $likes= $this->Likes->find('all')->where(['post_id IN '=>$postsId])->count();    
        }
        else{
            $likes=0;    
        }
        
        $posts= $this->Posts->find('all',['contain'=>['Users'=>['UserMedias'],'Comments'=>['Users'], 'Likes']])->where(['user_id'=>$model->id]);

        $this->set(compact('model','postsCount','photosCount','videosCount','likes','posts'));
    }//end follow


    

}
