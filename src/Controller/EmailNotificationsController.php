<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EmailNotifications Controller
 *
 * @property \App\Model\Table\EmailNotificationsTable $EmailNotifications
 *
 * @method \App\Model\Entity\EmailNotification[] paginate($object = null, array $settings = [])
 */
class EmailNotificationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $emailNotifications = $this->paginate($this->EmailNotifications);

        $this->set(compact('emailNotifications'));
        $this->set('_serialize', ['emailNotifications']);
    }

    /**
     * View method
     *
     * @param string|null $id Email Notification id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailNotification = $this->EmailNotifications->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('emailNotification', $emailNotification);
        $this->set('_serialize', ['emailNotification']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $emailNotification = $this->EmailNotifications->newEntity();
        if ($this->request->is('post')) {
            $emailNotification = $this->EmailNotifications->patchEntity($emailNotification, $this->request->getData());
            if ($this->EmailNotifications->save($emailNotification)) {
                $this->Flash->success(__('The email notification has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The email notification could not be saved. Please, try again.'));
        }
        $users = $this->EmailNotifications->Users->find('list', ['limit' => 200]);
        $this->set(compact('emailNotification', 'users'));
        $this->set('_serialize', ['emailNotification']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Notification id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $emailNotification = $this->EmailNotifications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailNotification = $this->EmailNotifications->patchEntity($emailNotification, $this->request->getData());
            if ($this->EmailNotifications->save($emailNotification)) {
                $this->Flash->success(__('The email notification has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The email notification could not be saved. Please, try again.'));
        }
        $users = $this->EmailNotifications->Users->find('list', ['limit' => 200]);
        $this->set(compact('emailNotification', 'users'));
        $this->set('_serialize', ['emailNotification']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Notification id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailNotification = $this->EmailNotifications->get($id);
        if ($this->EmailNotifications->delete($emailNotification)) {
            $this->Flash->success(__('The email notification has been deleted.'));
        } else {
            $this->Flash->error(__('The email notification could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
