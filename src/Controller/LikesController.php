<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[] paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Posts']
        ];
        $likes = $this->paginate($this->Likes);

        $this->set(compact('likes'));
        $this->set('_serialize', ['likes']);
    }

    /**
     * View method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $like = $this->Likes->get($id, [
            'contain' => ['Users', 'Posts']
        ]);

        $this->set('like', $like);
        $this->set('_serialize', ['like']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $like = $this->Likes->newEntity();
        if ($this->request->is('post')) {
            $like = $this->Likes->patchEntity($like, $this->request->getData());
            if ($this->Likes->save($like)) {
                $this->Flash->success(__('The like has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The like could not be saved. Please, try again.'));
        }
        return $this->redirect($this->referer());
        $users = $this->Likes->Users->find('list', ['limit' => 200]);
        $posts = $this->Likes->Posts->find('list', ['limit' => 200]);
        $this->set(compact('like', 'users', 'posts'));
        $this->set('_serialize', ['like']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $like = $this->Likes->find('all', [
            'contain' => ['Posts']
        ])->where(['post_id'=>$id]);
        pr($like); exit;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $like = $this->Likes->patchEntity($like, $this->request->getData());
            if ($this->Likes->save($like)) {
                $this->Flash->success(__('The like has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The like could not be saved. Please, try again.'));
        }
        $users = $this->Likes->Users->find('list', ['limit' => 200]);
        $posts = $this->Likes->Posts->find('list', ['limit' => 200]);
        $this->set(compact('like', 'users', 'posts'));
        $this->set('_serialize', ['like']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $like = $this->Likes->get($id);
        if ($this->Likes->delete($like)) {
            $this->Flash->success(__('The like has been deleted.'));
        } else {
            $this->Flash->error(__('The like could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function likeed($id=null){
        $this->loadModel('Posts');
        $likes = TableRegistry::get('Likes');

        $exists = $likes->exists(['post_id' => $id, 'user_id'=> $this->Auth->user('id')]);
        if($exists){
            //if exist
            $like = $this->Likes->find('all')->where(['post_id'=>$id, 'user_id'=> $this->Auth->user('id')])->first();
            if ($this->Likes->delete($like)) {
                $follower_id= $this->Posts->get($id);
                # create unliked notifications//'Liked','Unliked'
                $this->Common->createNotification($this->Auth->user('id'), $follower_id->user_id, 'Unliked', $this->Auth->user('fullname').' unliked your post. ');

                return $this->redirect($this->referer());
            }
        }
        else{
            //like add
            $like = $this->Likes->newEntity();
            $postLikes=[
                'post_id'=> $id,
                'user_id'=> $this->Auth->user('id'),
                'liked'=> 'Liked' ];
            $like = $this->Likes->patchEntity($like, $postLikes);
            if($this->Likes->save($like)) {
                $follower_id= $this->Posts->get($id);
                # create Liked notifications//'','Unliked'
                $this->Common->createNotification($this->Auth->user('id'), $follower_id->user_id, 'Liked', $this->Auth->user('fullname').' liked your post. ');

               return $this->redirect($this->referer());
            }
        }//end create like

    }//end 
}
