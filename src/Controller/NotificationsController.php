<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class NotificationsController extends AppController
{
      public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function index()
    { 
      $notifications= $this->Notifications->find('all')->where(['model_id'=>$this->Auth->user('id')])->order(['created'=>'DESC']); 
      $this->set(compact('notifications'));
    }

    public function interactions(){
      $notifications= $this->Notifications->find('all')->where(['model_id'=>$this->Auth->user('id'),'notification_type IN'=>['Subscription','Unsubscribe']])->order(['created'=>'DESC']); 
      $this->set(compact('notifications'));
    }

    public function subscribed(){
      $notifications= $this->Notifications->find('all')->where(['model_id'=>$this->Auth->user('id'),'notification_type IN'=>['Subscription','Unsubscribe']])->order(['created'=>'DESC']); 
      $this->set(compact('notifications'));
    }

    public function liked(){
      $notifications= $this->Notifications->find('all')->where(['model_id'=>$this->Auth->user('id'),'notification_type IN'=>['Liked','Unliked']])->order(['created'=>'DESC']); 
      $this->set(compact('notifications'));
    }

    public function pricechanged(){
      $notifications= $this->Notifications->find('all')->where(['model_id'=>$this->Auth->user('id'),'notification_type IN'=>['Price Chanhged']])->order(['created'=>'DESC']); 
      $this->set(compact('notifications'));
    }
    //    'Liked','Unliked','Tippedd','','Interaction'

    public function poke($model_id=null)
    {
        $this->loadmodel('Users');
        $currentusers = $this->Users->get($this->Auth->user('id'));
        pr($currentusers);
        exit;
        $notification = TableRegistry::get('Notifications');
        $createNotification= $notification->newEntity([
          'user_id' =>$this->Auth->user('id'),
          'model_id' => $model_id,
         // 'post_id'=> $post_id,
          'notification_type'=> 'Interaction',
          'message'=> '',
          'status'=> 'Unread',
          'created' => date("Y-m-d H:i:s"),
          'modified' => date("Y-m-d H:i:s")
      ]);
        /*if($createNotification=$notification->save($createNotification)){
          $settings=TableRegistry::get('Users');
          $get_user_details = $settings->find('all')->where(['id'=>$model_id])->first();
          $this->sendgloblemessage($get_user_details['email'],$message);
          return true;

        }
        else{
           return false;
        }*/

    }//end Notification




}
